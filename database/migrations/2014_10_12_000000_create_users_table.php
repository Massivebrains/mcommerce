<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_id')->nullable();
            $table->string('neolife_id')->nullable();
            $table->string('phone')->nullable();
            $table->string('name')->nullable();
            $table->string('country')->nullable();
            $table->string('country_no')->nullable();
            $table->string('country_code')->nullable();
            $table->string('currency_code')->nullable();
            $table->string('status')->nullable();
            $table->enum('type', ['user', 'admin'])->nullable()->default('user');
            $table->integer('access_level')->nullable()->default(0)->comment('0:Manager 1:Admin');
            $table->string('telegram_id')->nullable();
            $table->string('telegram_name')->nullable();
            $table->string('telegram_username')->nullable();
            $table->datetime('expire_at')->nullable();
            $table->timestamps();

            $table->index('phone');
            $table->index('neolife_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
