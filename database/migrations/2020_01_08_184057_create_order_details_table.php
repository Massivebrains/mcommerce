<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_id');
            $table->string('item_code')->nullable();
            $table->string('description')->nullable();
            $table->float('quantity')->nullable();
            $table->float('price')->nullable();
            $table->string('total')->nullable();
            $table->float('tax')->nullable();
            $table->float('pv')->nullable();
            $table->float('bv')->nullable();
            $table->timestamps();

            $table->index('order_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
