<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('group_order_id')->nullable();
            $table->text('log')->nullable();
            $table->timestamps();

            $table->index('order_id');
        });
    }


    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
