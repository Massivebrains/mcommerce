<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference')->nullable();
            $table->string('payment_reference')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('neolife_id')->nullable();
            $table->string('name')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('payment_provider_id')->nullable();
            $table->integer('group_order_id')->nullable();
            $table->double('shipping')->nullable();
            $table->string('total')->nullable(); //having numeric overflow problems
            $table->double('tax')->nullable();
            $table->double('pv')->nullable();
            $table->double('bv')->nullable();
            $table->string('transaction_reference')->nullable();
            $table->string('country_code')->nullable();
            $table->string('exigo_order_id')->nullable();
            $table->text('callback_url')->nullable();
            $table->text('return_url')->nullable();
            $table->text('description')->nullable();
            $table->enum('status', ['incomplete', 'pending', 'paid', 'processing', 'processed', 'failed_to_process', 'cancelled'])->default('incomplete');
            $table->enum('type', ['single', 'master', 'child', 'web'])->nullable()->default('single');
            $table->enum('source', ['sms', 'whatsapp', 'telegram', 'web'])->nullable()->default('sms');
            $table->datetime('paid_at')->nullable();
            $table->timestamps();

            $table->index('reference');
            $table->index('created_at');
            $table->index(['status', 'created_at']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
