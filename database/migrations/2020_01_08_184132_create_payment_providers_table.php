<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentProvidersTable extends Migration
{
    public function up()
    {
        Schema::create('payment_providers', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->string('country_code')->nullable();
            $table->string('name')->nullable();
            $table->string('provider')->nullable();
            $table->text('message')->nullable();
            $table->float('charge')->nullable();
            $table->text('api_token')->nullable();
            $table->enum('status', ['active', 'inactive'])->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('payment_providers');
    }
}
