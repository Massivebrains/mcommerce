<?php

use Illuminate\Database\Seeder;

use App\Location;
use App\PaymentProvider;
use App\Services\Exigo;

class DatabaseSeeder extends Seeder
{
    public $payments;

    public function __construct()
    {
        $this->payments = [

            [
                'code'          => 4,
                'country_code'  => 'NG',
                'name'          => 'GTBank',
                'provider'      => null,
                'message'       => 'Dial *737*50*{amount}*001# to Pay.',
                'charge'        => 0,
                'api_token'     => '02e29f9ccc7571c8cc85edb2d0042c10',
                'status'        => 'active'
            ],

            [
                'code'          => 5,
                'country_code'  => 'NG',
                'name'          => 'ZenithBank',
                'provider'      => null,
                'message'       => 'Dial *966*6*04653*{reference}# to Pay.',
                'charge'        => 0,
                'api_token'     => '3d0a90fad7b88d1b746bc52b89e27401',
                'status'        => 'active'
            ],

            [
                'code'          => 6,
                'country_code'  => 'NG',
                'name'          => 'FBN',
                'provider'      => null,
                'message'       => 'Dial *894*89400007*{amount}# to Pay.',
                'charge'        => 20,
                'api_token'     => 'fb332f75497dfb96a233cffc0399d4db',
                'status'        => 'active'
            ],

            [
                'code'          => 7,
                'country_code'  => 'NG',
                'name'          => 'OTHER BANKS',
                'provider'      => null,
                'message'       => 'Dial *402*89400007*{amount}# to Pay.',
                'charge'        => 20,
                'api_token'     => 'b6be0a4dcb049b8c83af63b8e705e953',
                'status'        => 'active'
            ],

            [
                'code'          => 8,
                'country_code'  => 'NG',
                'name'          => 'Paystack',
                'provider'      => 'Paystack',
                'message'       => 'Use this link to pay: {url}',
                'charge'        => 0,
                'api_token'     => 'b6be0a4dcb4329b8c53444',
                'status'        => 'active'
            ],
            [
                'code'          => 9,
                'country_code'  => 'GH',
                'name'          => 'Interpay',
                'provider'      => null,
                'provider'      => 'Interpay',
                'message'       => 'Use this link to pay: {url}',
                'charge'        => 0,
                'api_token'     => '4yPVfAaEc7bImPmGGqav8',
                'status'        => 'active'
            ],

            [
                'code'          => 10,
                'country_code'  => 'GH',
                'name'          => 'MTN',
                'provider'      => null,
                'provider'      => 'MTN',
                'message'       => 'Use this link to pay: {url}',
                'charge'        => 0,
                'api_token'     => 'HXp0P342FYBtK53PUZyYlm0Vqwm',
                'status'        => 'active'
            ]
        ];
    }

    public function run()
    {
        //$this->seedLocations();
        $this->updateShipLocations();
        //PaymentProvider::insert($this->payments);
    }

    public function seedLocations($country = 'NG')
    {
        $response = Exigo::get('shiplocations');

        $starting_code = Location::orderBy('code', 'desc')->first()->code;

        foreach ($response->data as $row) {
            if (in_array($row->CountryCode, ['NG', 'GH'])) {
                continue;
            }

            Location::updateOrCreate(['name' => $row->Name, 'country_code' => $row->CountryCode], [

                'code'              => $starting_code++,
                'name'              => $row->Name,
                'warehouse_id'      => $row->WarehouseId,
                'warehouse_name'    => $row->WarehouseName,
                'time_zone'         => $row->TimeZone,
                'currency'          => $row->Currency,
                'country_no'        => $row->CountryNumber,
                'country_code'      => $row->CountryCode,
                'country_name'      => $row->CountryName,
                'country_region'    => $row->CountryRegion,
                'ship_method'       => $row->Id,
                'status'            => 'active'
            ]);
        }
    }

    public function updateShipLocations()
    {
        $response = Exigo::get('shiplocations');

        foreach ($response->data as $row) {
            if (!in_array($row->CountryCode, ['NG'])) {
                continue;
            }

            $location = Location::where(['name' => $row->Name, 'country_code' => $row->CountryCode, 'ship_method' => null])->first();

            if ($location) {
                $location->update(['ship_method' => $row->Id]);
            }
        }
    }
}
