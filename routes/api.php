<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::any('/fallback', 'MessageController@fallback');

    Route::get('/sms/{phone}/{message}/{source?}', 'MessageController@sms');
    Route::post('/whatsapp', 'MessageController@whatsapp');
    Route::post('/telegram', 'MessageController@telegram');
    Route::any('/telegram-empty', 'MessageController@telegramEmpty');
    Route::get('/telegram/webhook/get', 'MessageController@getWebhook');
    Route::get('/telegram/webhook/set', 'MessageController@setWebhook');

    Route::post('/pay', 'OrderController@pay');

    Route::group(['prefix' => 'payments', 'namespace' => 'Payments'], function () {
        Route::group(['prefix' => 'validation', 'namespace' => 'Validation'], function () {
            Route::any('amount', OrderAmountController::class);
            Route::any('reference', OrderReferenceController::class);
        });

        Route::group(['namespace' => 'Gateways'], function () {
            Route::post('ussd/callback', USSDController::class);
        });
    });
});
