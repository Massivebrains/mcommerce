<?php

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;

Route::get('/', 'Admin\AuthController@index');
Route::get('/login', 'Admin\AuthController@index')->name('login');
Route::post('/login', 'Admin\AuthController@login');
Route::get('/logout', 'Admin\AuthController@logout');

Route::group(['prefix' => 'web'], function () {
    Route::get('form', 'WebPaymentController@form');
    Route::post('pay', 'WebPaymentController@pay');
    Route::get('requery/{id?}/{simulate?}', 'WebPaymentController@requery');
});

Route::any('/p/{payment_reference?}', 'Api\Payments\Gateways\MDWBankController@pay');

Route::group(['prefix' => 'payments', 'namespace' => 'Api\Payments'], function () {
    Route::group(['prefix' => 'mdw', 'namespace' => 'Gateways'], function () {
        Route::get('pay/{payment_reference?}', 'MDWBankController@pay');
        Route::get('return-url', 'MDWBankController@return');
        Route::get('callback-url', 'MDWBankController@callback');
    });
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');

    Route::group(['namespace' => 'Orders'], function () {
        Route::get('/orders/single', 'OrdersController@single');
        Route::get('/orders/group', 'OrdersController@group');
        Route::get('/orders/web', 'OrdersController@web');
        Route::post('/orders/search', 'OrdersController@search');
        Route::get('/order/{id}', 'OrderController@index');
        Route::get('/order/{id}/retry', 'OrderController@retry');
        Route::get('/order/{id}/ignore', 'OrderController@ignore');
        Route::get('/order/{id}/resolve-payment', 'OrderController@resolvePaymentForm');
        Route::post('/order/{id}/resolve-payment', 'OrderController@resolvePayment');
    });

    Route::get('/locations', 'LocationsController@index');
    Route::get('/payment-providers', 'PaymentProvidersController@index');
    Route::get('/invoice', 'InvoiceController@index');
    Route::post('/invoice/save', 'InvoiceController@save');

    Route::get('conversations/{source?}', 'ConversationsController@index');
    Route::get('conversation/{source?}/{user_id?}', 'ConversationsController@conversation');
    Route::post('send-message', 'ConversationsController@sendMessage');
    Route::get('web-conversation', 'WebConversationController@index');
    Route::get('web-conversation/get-user/{neolife_id}', 'WebConversationController@getUser');
    Route::post('web-conversation/message', 'WebConversationController@message');
});

Route::get('/t', 'WebPaymentController@ussdLink');

Route::get('redis', function () {
    Redis::command('flushdb');
    return response()->json(true);
});

Route::get('/migrate', function () {
    $paid = \App\Order::whereStatus('paid')->whereIn('type', ['single', 'child'])->get();
    $order_ids = [];
    foreach ($paid as $row) {
        \App\Jobs\ProcessOrderOnExigo::dispatch($row);
        $order_ids[] = $row->reference;
    }
    return response()->json([
        'status' => 'Successful',
        'message' => 'Paid orders pushed back to queue',
        'data' => [
            'order_ids' => $order_ids
        ]
    ]);
});
