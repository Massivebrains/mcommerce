var app = new Vue({

	el: '#web-messaging',

	data: {

		user: null,

		loading: false,
		message: '',
		error: '',
		neolife_id: '',
		time: 0,
		messages: []

	},

	methods: {

		getUser(){

			try{

				this.loading = true;

				if(this.neolife_id.length < 7){

					this.loading = false;
					return this.error = 'Invalid Neolife ID';
				}

				axios.get(`/admin/web-conversation/get-user/${this.neolife_id}`).then(response => {

					let data = response.data;

					if(data.status == false){

						this.error = data.data;
						
					}else{

						this.user = data.data;
					}

					this.loading = false;

				}).catch(error => {

					this.error = error;
					this.loading = false;
				})

			}catch(error){

				console.log(error)
			}
		},

		send(){

			this.time = 25;
			this.loading = true;

			this.counter();

			try{


				let payload = {

					phone: this.user.phone,
					message: this.message
				};

				this.messages.push({type: 'message', text: this.message });

				axios.post('/admin/web-conversation/message', payload).then(response => {

					this.scroll(response.data.original);

				}).catch(error => {

					this.scroll(error.original);
				})

			}catch(error){

				this.scroll(error.original);
			}
		},

		counter(){

			let interval = setInterval(() => {

				this.time -= 1;

				if(this.time == 0)
					clearInterval(interval);

			}, 1000);
		},

		scroll(message = ''){

			this.messages.push({type: 'reply', text: message});
			this.message = '';
			this.loading = false;
			
			setTimeout(() => {

				this.$refs['chat'].scrollTop = this.$refs['chat'].scrollHeight;

			}, 500)
		}
	}

})