<?php

return [
   
    'welcome' 					=> 'HI :name, Your Neolife ID is :neolife_id',
    'group_orders_cancelled'	=> 'All your previously incompleted group orders have now been cancelled. You can now send a correct order to start a new group order.',
    'telegram_start'			=> 'Hi there!, To get started Please reply with: NEO JOIN PHONENUMBER (e.g NEO JOIN 2348100000000). make sure you include your country code eg 234 for Nigeria.',
    'joined_successfully'       => 'Congratulations! You have now linked your Telegram account with me. Go ahead and start placing orders!!!'

];
