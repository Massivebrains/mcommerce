<?php

return [

	'exception' 						=> 'We ran into some technical Error. Please try again. (:error)',
	'invalid_customer' 					=> 'Your phone number is not registered with Neolife. Kindly Contact any of Our offices.',
	'invalid_command'					=> 'Invalid Command',
    'invalid_format'                    => 'Your order is in the wrong format.',
    'invalid_format_missing_code'       => 'Your order is in the wrong format. One of NEO BUY, ID, DC or BANK is missing.',
    'invalid_format_no_incomplete'		=> 'Your order is in the wrong format. Please resend Order.',
    'invalid_neolife_id'                => 'Invalid Neolife ID.',
    'invalid_international_neolife_id'	=> 'Invalid Neolife ID. The correct Neolife ID format is XXX-XXXXXXX e.g 063-1234567',
    'invalid_location'					=> 'Invalid Location',
    'invalid_bank'						=> 'Invalid Bank',
    'invalid_products_format'			=> 'Your products are in the wrong format',
    'inactive_location'					=> 'Your selected location is currently not taking orders',
    'inactive_bank'						=> 'Your selected Bank is currenlty not avaliable to process payments.',
    'invalid_for_id'					=> 'The Neolife ID you are trying to buy for is Invalid or may have expired.',
    'invalid_group_format'              => 'Your group order is in the wrong format.',
    'invalid_multipls_countries'        => 'You must choose locations that belong in the same country withing a group order. Please resend a correct group order or send CANCEL to start over.',
    'invalid_multiple_banks'            => 'You must select the same Bank within a group order. Please resend a correct group order or send CANCEL to start over.',
    'no_children_in_group_order'        => 'There are no orders within this group order yet. Send a correct group order to add a group order',
    'expired_distributor'               => 'Distributor is currently expired.',

    'mdw_invalid_order'                 => 'Invalid Order ID provided for payment.',
    'mdw_already_paid'                  => 'This order has already been paid for.',

    'invalid_neo_join'                  => 'Your NEO JOIN command is invalid. Correct format is NEO JOIN PHONENUMBER. (e.g NEO JOIN 2348000000000) make sure you include your country code eg 234 for Nigeria',
    'already_joined_telegram'           => 'You have already linked your telegram account with me.',
    'phone_number_link_invalid'         => 'Opzz! Looks like someone else has linked this phone number to another Telegram account. Please contact one of our offices for assistance.'
];
