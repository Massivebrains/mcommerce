<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{ $title ?? 'Mcommerce' }}</title>

    <meta name="description" content="Mcommerce">
    <meta name="author" content="Olaiya Segun <oluwaseguno@ng.neolife.com>">
    <meta name="robots" content="noindex, nofollow">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/main.min.css">

    @yield('styles')

</head>

<body>

    <div id="page-container">


        @php
            $active = isset($active) ? $active : '';
        @endphp

        <div class="bg-image" style="background-image: url('/media/various/bg.png');">
            <div class="bg-image-overlay py-5">
                <header class="container d-md-flex align-items-md-center justify-content-md-between py-4">
                    <div class="text-center text-md-left py-3">
                        <a class="h4 text-dark font-weight-600" href="">
                            <i class="far fa-fw fa-circle text-success mr-1"></i> Mcommerce | {{ Auth::check() ? Auth::user()->country_code : 'Africa' }}
                        </a>
                    </div>
                    <div class="text-center text-md-right py-3">
                        <div class="dropdown">
                            <button class="btn btn-sm btn-success px-3 py-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-fw fa-user mr-1"></i> {{ Auth::check() ? Auth::user()->name : 'Administrator' }} <i class="fa fa-fw fa-chevron-down ml-1"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right font-size-sm" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="/horizon" target="_blank">Horizon</a>
                                <a class="dropdown-item" href="/telescope" target="_blank">Telescope</a>
                                <a class="dropdown-item" href="/logout">Logout</a>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        </div>


        <div class="container py-4">
            <div class="row">
                <div class="col-lg-4 col-xl-3">

                    <button type="button" class="btn btn-secondary btn-block d-lg-none mb-4" onclick="$('#navigation').toggleClass('d-none');">
                        <i class="fa fa-fw fa-bars mr-1"></i> Navigation
                    </button>


                    <div id="navigation" class="block d-none d-lg-block mr-lg-4">
                        <div class="block-content">
                            <nav class="nav nav-pills flex-column mb-0">

                                <div class="font-size-sm text-uppercase text-black-50 font-weight-bold mb-3">
                                    Main
                                </div>

                                @php($carbon = new \Carbon\Carbon())
                                @php($start_date = $carbon->startOfMonth()->format('Y-m-d'))
                                @php($end_date = $carbon->endOfMonth()->format('Y-m-d'))

                                @if (Auth::user()->access_level > 0)
                                    <a class="nav-link mb-2" href="https://metabase.neolifeafrica.info/public/dashboard/e5f12209-2e2c-4539-b799-46b98042bab2?country={{ Auth::user()->country_code }}&start_date={{ $start_date }}&end_date={{ $end_date }}" target="_blank">
                                        <i class="fa fa-fw fa-chart-line mr-1"></i>
                                        Dashboard
                                    </a>
                                @endif

                                <a class="nav-link mb-2 {{ $active == 'locations' ? 'active' : '' }}" href="/admin/locations">
                                    <i class="fa fa-fw fa-compass mr-1"></i>
                                    Locations
                                </a>

                                <a class="nav-link mb-2 {{ $active == 'banks' ? 'active' : '' }}" href="/admin/payment-providers">
                                    <i class="fa fa-fw fa-university mr-1"></i>
                                    Banks
                                </a>

                                @if (Auth::user()->access_level > 0)
                                    <a class="nav-link mb-2 {{ $active == 'invoice' ? 'active' : '' }}" href="/admin/invoice">
                                        <i class="fa fa-fw fa-file mr-1"></i>
                                        Monthly Invoice
                                    </a>
                                @endif

                                <div class="font-size-sm text-uppercase text-black-50 font-weight-bold mb-3 mt-4">
                                    Orders
                                </div>

                                <a class="nav-link mb-2 {{ $active == 'single-orders' ? 'active' : '' }}" href="/admin/orders/single">
                                    <i class="fa fa-fw fa-cube mr-1"></i>
                                    Singles
                                </a>

                                <a class="nav-link mb-2 {{ $active == 'group-orders' ? 'active' : '' }}" href="/admin/orders/group">
                                    <i class="fa fa-fw fa-users mr-1"></i>
                                    Groups
                                </a>

                                @if (Auth::user()->access_level > 0)
                                    <a class="nav-link mb-2 {{ $active == 'web-orders' ? 'active' : '' }}" href="/admin/orders/web">
                                        <i class="fa fa-fw fa-money-bill mr-1"></i>
                                        Web
                                    </a>
                                @endif

                                @if (Auth::user()->access_level > 0)
                                    <div class="font-size-sm text-uppercase text-black-50 font-weight-bold mb-3 mt-4">
                                        Conversations
                                    </div>

                                    <a class="nav-link mb-2 {{ $active == 'sms' ? 'active' : '' }}" href="/admin/conversations/sms">
                                        <i class="fa fa-fw fa-phone mr-1"></i>
                                        SMS
                                    </a>

                                    <a class="nav-link mb-2 {{ $active == 'whatsapp' ? 'active' : '' }}" href="/admin/conversations/whatsapp">
                                        <i class="fa fa-fw fa-phone mr-1"></i>
                                        Whatsapp
                                    </a>

                                    <a class="nav-link mb-2 {{ $active == 'telegram' ? 'active' : '' }}" href="/admin/conversations/telegram">
                                        <i class="fa fa-phone mr-1"></i>
                                        Telegram
                                    </a>

                                    <a class="nav-link mb-2 {{ $active == 'web' ? 'active' : '' }}" href="/admin/web-conversation">
                                        <i class="fa fa-globe mr-1"></i>
                                        Web
                                    </a>
                                @endif

                            </nav>
                        </div>
                    </div>

                </div>

                <div class="col-lg-8 col-xl-9">

                    @include('partials._alert')

                    @yield('content')

                </div>
            </div>
        </div>



        <div class="bg-white">
            <footer class="container font-size-sm d-md-flex justify-content-md-between py-4">
                <div class="text-center text-md-left py-2">
                    <script>
                        document.write((new Date()).getFullYear());
                    </script>
                </div>
                <div class="text-center text-md-right py-2">
                    Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600" target="_blank" href="https://ogaranya.com">Ogaranya</a>
                </div>
            </footer>
        </div>

    </div>

    <script src="https://code.jquery.com/jquery-3.4.0.slim.min.js" integrity="sha256-ZaXnYkHGqIhqTbJ6MB4l9Frs/r7U4jlx7ir8PJYBqbI=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    @yield('scripts')

</body>

</html>
