<noscript>
    <meta http-equiv="refresh" content="0; URL=/web/pay/noscript">
</noscript>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <link rel="stylesheet" href="/web/styles.css">
    <title>NEOLIFE INTERNATIONAL - OGARANYA PAYMENT</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
</head>

<body data-sa-theme="3">

    @yield('content')

</body>

@yield('scripts')

</html>
