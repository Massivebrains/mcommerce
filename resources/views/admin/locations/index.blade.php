@extends('layouts.app')

@section('content')

<div class="block">
    <div class="block-content bg-light">
        <h3 class="font-size-sm text-uppercase mb-0">Locations</h3>
    </div>
    <div class="block-content">

        <div class="table-responsive">
            <table class="table table-borderless table-striped table-vcenter font-size-sm mb-0">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Warehouse</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($locations as $row)
                    <tr>
                        <td>{{$row->code}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->warehouse_name}}</td>
                        <td>{{_badge($row->status)}}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

        <div class="m-1">
            {{ $locations->links() }}
        </div>

    </div>
</div>

@endsection