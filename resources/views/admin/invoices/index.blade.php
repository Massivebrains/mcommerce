@extends('layouts.app')

@section('content')

<div class="block">
    <div class="block-content bg-light">
        <h3 class="font-size-sm text-uppercase mb-0">Generate Invoice</h3>
    </div>
    <div class="block-content">

        <form method="POST" role="form" action="/admin/invoice/save">

            {{csrf_field()}}

            <div class="form-group">
                <label>Start Date</label>
                <input type="text" class="form-control" name="start_at" value="{{date('Y-m-d H:i:s')}}" required>
                @error('start_at')
                    <p class="text-danger">{{$errors->first('start_at')}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label>End Date</label>
                <input type="text" class="form-control" name="end_at" value="{{date('Y-m-d H:i:s')}}" required>
                 @error('end_at')
                    <p class="text-danger">{{$errors->first('end_at')}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label>Description</label>
                <input type="text" class="form-control" name="description" placeholder="Description" required>
                 @error('description')
                    <p class="text-danger">{{$errors->first('description')}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label>Country Code</label>
                <input type="text" class="form-control" name="country_code" placeholder="Country code" value="NG" required>
                 @error('country_code')
                    <p class="text-danger">{{$errors->first('country_code')}}</p>
                @enderror
            </div>
                
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@endsection