<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Invoice {{ $description }}</title>
    <style type="text/css">
        * {
            font-family: arial;
            font-size: 10px;
        }

        body {
            margin: 10px;
        }

        .container {
            padding: 10px;
        }

        .row {
            margin: 0;
            position: relative;
        }

        .col-md-8 {
            float: left;
            width: 66.66666667%;
        }

        .col-md-4 {
            float: left;
            width: 33.33333333%;
        }

        .text-primary {
            color: #337ab7;
        }

        a.text-primary:hover {
            color: #286090;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        .h1,
        .h2,
        .h3,
        .h4,
        .h5,
        .h6 {
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }

        table {
            background-color: transparent;
        }

        caption {
            padding-top: 8px;
            padding-bottom: 8px;
            color: #777;
            text-align: left;
        }

        th {
            text-align: left;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>th,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>tbody>tr>td,
        .table>tfoot>tr>td {
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .table>thead>tr>th {
            vertical-align: bottom;
            border-bottom: 2px solid #ddd;
        }

        .table>caption+thead>tr:first-child>th,
        .table>colgroup+thead>tr:first-child>th,
        .table>thead:first-child>tr:first-child>th,
        .table>caption+thead>tr:first-child>td,
        .table>colgroup+thead>tr:first-child>td,
        .table>thead:first-child>tr:first-child>td {
            border-top: 0;
        }

        .table>tbody+tbody {
            border-top: 2px solid #ddd;
        }

        .table .table {
            background-color: #fff;
        }

    </style>
</head>

<body>
    <div class="container">
        <table class="table" style="font-size:10px !important;">
            <tr>
                <td style="width:50%">
                    <h4 class="text-primary" style="font-weight: bold">Think First Technologies Limited</h4>
                    <table>
                        <tbody>
                            <tr>
                                <td><strong>TO &nbsp;</strong></td>
                                <td>Adenrele John-Adeyemi</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td style="width:50%">
                    <h2 class="text-primary">INVOICE | {{ $description }}</h2>
                    12a Mabinuori Dawodu street, </br>
                    Gbagada Phase 1.<br />
                    <a href="mailto:think@ogaranya.com?" "think@ogaranya.com">think@ogaranya.com</a>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td style=""><strong>INVOICE NUMBER &nbsp;</strong></td>
                                <td>OGGN00-{{ mt_rand(111111, 999999) }}</td>
                            </tr>
                            <tr>
                                <td><strong>DATE</strong></td>
                                <td>{{ date('Y-m-d') }}</td>
                            </tr>
                            <tr>
                                <td><strong>SHIP TO</strong></td>
                                <td>Adenrele John-Adeyemi<br>GNLD International<br>12, Olu Oshunkeye Street,<br>Gbagada, Lagos.</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

        </table>

        <div class="row">
            <div class="col-md-12">
                <table class="table" style="font-size:10px !important">
                    <caption>
                        <h4>Job Payment Terms : Dues on Receipt</h4>
                    </caption>
                    <thead>
                        <tr>
                            <th>JOB</th>
                            <th>UNIT</th>
                            <th>QUANTITY</th>
                            <th>TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Web Commission due to ThinkFirst technologies limited ({{ $country }})</td>
                            <td>&nbsp;</td>
                            <td>{{ number_format($web->count(), 2) }}</td>
                            <td><strong>{{ number_format($web_sum) }}</strong></td>
                        </tr>
                        <tr>
                            <td>Orders Commission due to ThinkFirst technologies limited ({{ $country }})</td>
                            <td>{{ number_format(75) }}</td>
                            <td>{{ number_format($orders_count, 2) }}</td>
                            <td><strong>{{ number_format($orders_count * 75, 2) }}</strong></td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <th>&nbsp;</th>
                            <th>{{ number_format($orders_count + $web->count(), 2) }}</th>
                            <th><strong>{{ number_format($orders_count * 75 + $web_sum, 2) }}</strong></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
