@extends('layouts.app')

@section('content')

<div class="block">
    <div class="block-content bg-light">
        <h3 class="font-size-sm text-uppercase mb-0">Payment Providers</h3>
    </div>
    <div class="block-content">

        <div class="table-responsive">
            <table class="table table-borderless table-striped table-vcenter font-size-sm mb-0">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Charge</th>
                        <th>Status</th>
                        <th>Message</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($payment_providers as $row)
                    <tr>
                        <td>{{$row->code}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{_c($row->charge)}}</td>
                        <td>{{_badge($row->status)}}</td>
                        <td>{{$row->message}}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

        <div class="m-1">
            {{ $payment_providers->links() }}
        </div>

    </div>
</div>

@endsection