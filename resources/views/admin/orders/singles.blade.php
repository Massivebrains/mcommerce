@extends('layouts.app')

@section('content')

    @include('partials._search')

    <div class="block">
        <div class="block-content bg-light">
            <h3 class="font-size-sm text-uppercase mb-0">Single Orders</h3>
        </div>
        <div class="block-content">

            <div class="table-responsive">
                <table class="table table-borderless table-hover table-vcenter font-size-sm mb-0">
                    <thead>
                        <tr>
                            <th style="width: 40px;">OrderID</th>
                            <th>NeoLifeID</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Source</th>
                            <th>Bank</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($orders as $row)
                            <tr style="cursor:pointer" onclick="window.location = '/admin/order/{{ $row->id }}'">
                                <td>{{ $row->reference }}</td>
                                <td>{{ $row->neolife_id }}</td>
                                <td>{{ _c($row->total) }}</td>
                                <td>{{ _badge($row->status) }}</td>
                                <td>{{ _badge($row->source) }}</td>
                                <td>{{ $row->payment_provider->name }}</td>
                                <td><small>{{ _d($row->created_at) }}</small></td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>

            <div class="m-1">
                {{ $orders->links() }}
            </div>
        </div>

    </div>

@endsection
