@extends('layouts.app')

@section('content')

    @include('partials._search')

    <div class="block">
        <div class="block-content bg-light">
            <h3 class="font-size-sm text-uppercase mb-0">Search Results</h3>
        </div>
        <div class="block-content">

            <div class="table-responsive">

                @if ($orders->count() > 0)

                    <div class="alert alert-success">{{ $orders->count() }} Orders match your search query.</div>
                    @if($print == true)
                        <small><code>[If your browser does not automatically open a new print tab, kindly enable popup. See <a href="https://monosnap.com/file/TpuxpXMFXHDazM02P1mPE0nTAdguLS" target="_blank">https://monosnap.com/file/TpuxpXMFXHDazM02P1mPE0nTAdguLS</a> </code></small>
                     @endif

                    <table class="table table-borderless table-striped table-vcenter font-size-sm mb-0">
                        <thead>
                            <tr>
                                <th style="width: 40px;">OrderID</th>
                                <th>NeoLifeID</th>
                                <th>ExigoID</th>
                                <th>Type</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($orders as $row)
                                <tr style="cursor:pointer" onclick="window.location = '/admin/order/{{ $row->id }}'">
                                    <td>{{ $row->reference }}</td>
                                    <td>{{ $row->neolife_id }}</td>
                                    <td>{{ $row->exigo_order_id }}</td>
                                    <td>{{ _badge($row->type) }}</td>
                                    <td>{{ _c($row->total) }}</td>
                                    <td>{{ _badge($row->status) }}</td>
                                    <td>{{ _d($row->created_at) }}</td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                @else
                    <div class="alert alert-info">
                        Your search returned zero results.
                    </div>
                @endif
            </div>

            <div class="m-1">
                {{ $orders->links() }}
            </div>

        </div>
    </div>

@endsection

@section('scripts')

    @if($orders->count() > 0 && $orders->first()->exigo_order_id)
    <script>
        let print = Boolean({{ $print }})
        
        if (print == true) {
            window.open(`https://office.neolifeafrica.info/invoice?order_id={{ $orders->first()->exigo_order_id }}`,
                '_blank')
        }

    </script>
    @endif
@endsection
