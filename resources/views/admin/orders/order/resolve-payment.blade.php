@extends('layouts.app')

@section('content')

<div class="block">

    <div class="block-content bg-light">
        <h3 class="font-size-sm text-uppercase mb-0">Resolve Payment for order {{$order->reference}}</h3>
    </div>

    <div class="block-content">

        <form action="/admin/order/{{$order->id}}/resolve-payment" method="POST" role="form">

            {{csrf_field()}}

            <div class="form-group">
             <label>Order ID</label>
             <input type="text" class="form-control" value="{{$order->reference}}" disabled readonly>
             <input type="hidden" name="id" value="{{$order->id}}">
         </div>

         <div class="form-group">
             <label>Payment Provider</label>
             <select name="payment_provider_id" class="form-control" required>
                @foreach($payment_providers as $row)
                @if($row->id == $order->payment_provider->id)
                <option value="{{$row->id}}" selected>{{$row->name}}</option>
                @else
                <option value="{{$row->id}}">{{$row->name}}</option>
                @endif
                @endforeach
            </select>
            @if($errors->has('payment_provider_id'))
            <span class="text-danger">{{$errors->first('payment_provider_id')}}</span>
            @endif
        </div>

        <div class="form-group">
         <label>Payment Provider Reference</label>
         <input type="text" class="form-control" name="transaction_reference" placeholder="Payment Provider Reference Number" required>
         @if($errors->has('transaction_reference'))
         <span class="text-danger">{{$errors->first('transaction_reference')}}</span>
         @endif
     </div>

     <div class="form-group">
         <label>Payment Provider Description</label>
         <textarea name="description" class="form-control" rows="3" placeholder="Bank Statement Evidence" required></textarea>
         @if($errors->has('description'))
         <span class="text-danger">{{$errors->first('description')}}</span>
         @endif
     </div>

     <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure?')">Update Order</button>

 </form>

</div>
</div>

@endsection