@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="/css/timeline.css">
@endsection

@section('content')

    <div class="block">

        <div class="block-header bg-{{ $color }} p-2">

            <h4 class="block-title text-white">{{ $order->neolife_id }} {{ ucwords($order->type . ' Order') }}</h4>

        </div>

        <div class="block-content bg-light">

            <div class="row my-20">

                <div class="col-6">
                    <p class="h5">{{ $order->name }}</p>
                    <address>
                        <strong>Location:</strong> {{ $order->location->name }}<br>
                        <strong>Warehouse:</strong> {{ $order->location->warehouse_name }}
                    </address>
                </div>


                <div class="col-6 text-right">
                    <p class="h5">{{ $order->payment_provider->name }}</p>
                    <address>
                        <strong>Reference:</strong> {{ $order->transaction_reference }}<br>
                        <strong>Order Source:</strong> {{ _badge($order->source) }}<br>
                    </address>
                </div>

            </div>



            <div class="table-responsive push">

                <table class="table table-bordered table-striped table-sm bg-white">
                    <tbody>
                        @if ($order->type == 'child')
                            <tr>
                                <th>MasterOrderID</th>
                                <td><a href="/admin/order/{{ $order->master->id }}">{{ $order->master->reference }}</a>
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <th>Reference</th>
                            <td>{{ $order->reference }}</td>
                        </tr>

                        <tr>
                            <th>Status</th>
                            <td>{{ _badge($order->status) }}</td>
                        </tr>

                        @if (strlen($order->description) > 0)
                            <tr>
                                <th>Notes</th>
                                <td><code>{{ $order->description }}</code></td>
                            </tr>
                        @endif

                        <tr>
                            <th>ExigoID</th>
                            <td>{{ $order->exigo_order_id ?? 'N/A' }}</td>

                        </tr>

                        <tr>
                            <th>Total</th>
                            <td>{{ _c($order->total) }}</td>
                        </tr>

                        <tr>
                            <th>Total PV</th>
                            <td>{{ $order->pv ?? 'N/A' }}</td>
                        </tr>
                        <tr>
                            <th>Total BV</th>
                            <td>{{ $order->bv ?? 'N/A' }}</td>
                        </tr>
                        <tr>
                            <th>Date Created</th>
                            <td>{{ _d($order->created_at) }}</td>
                        </tr>
                        <tr>
                            <th>Date Paid</th>
                            <td>{{ _d($order->paid_at) }}</td>
                        </tr>

                        @if ($order->paid_at)
                            <tr>
                                <th>Time Taken</th>
                                <td>{{ $order->paid_at->diffForHumans($order->created_at) }}</td>
                            </tr>
                        @endif

                        @if (Auth::user()->access_level > 0)
                            <tr>
                                <th>Actions</th>
                                <td>
                                    @if ($order->exigo_order_id || $order->type == 'web')
                                        <a href="https://office.neolifeafrica.info/invoice?order_id={{ $order->exigo_order_id }}" class="btn btn-primary btn-sm" target="_blank">Print Exigo Invoice</a>
                                    @endif

                                    <a href="/admin/conversation/{{ $order->source }}/{{ $order->user_id }}" class="btn btn-info btn-sm">View Coversation</a>

                                    @if (in_array($order->type, ['single', 'child']) && ($order->status == 'failed_to_process' || $order->status == 'paid'))
                                        @if (session('message') == 'Order pushed back into the processing queue.')
                                            <button type="button" class="btn btn-primary btn-sm" disabled>Processing...</button>
                                        @else
                                            <a href="/admin/order/{{ $order->id }}/retry" class="btn btn-primary btn-sm">Retry</a>
                                        @endif
                                    @endif

                                    @if ($order->status == 'failed_to_process')
                                        <a href="/admin/order/{{ $order->id }}/ignore" class="btn btn-danger btn-sm" onclick="return confirm('By ignoring this order, it would be automatically marked as processed but this order will not be created on exigo. Are you sure you want to continue?')">Ignore</a>
                                    @endif


                                    @if ($order->status == 'pending')
                                        <a href="/admin/order/{{ $order->id }}/resolve-payment" class="btn btn-primary btn-sm">Resolve Payment</a>
                                    @endif

                                    @if ($order->payment_provider->provider && $order->status == 'pending')
                                        <a href="{{ url('p/' . $order->payment_reference) }}" class="btn btn-warning btn-sm">Requery</a>
                                    @endif

                                </td>
                            </tr>
                        @endif


                        @if ($order->type == 'web')
                            <tr>
                                <th>Descripton</th>
                                <td>{{ $order->description }}</td>
                            </tr>

                            <tr>
                                <th>Callback URL</th>
                                <td>{{ $order->callback_url }}</td>
                            </tr>

                            <tr>
                                <th>Return URL</th>
                                <td>{{ $order->return_url }}</td>
                            </tr>
                        @endif

                    </tbody>
                </table>
            </div>

            @if ($order->order_details->count() > 0)
                <h5>Products</h5>

                <div class="table-responsive push">

                    <table class="table table-bordered table-striped table-sm bg-white">
                        <thead>
                            <tr>
                                <th>Item Code</th>
                                <th>Description</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total</th>
                                <th>PV</th>
                                <th>BV</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($order->order_details as $row)
                                <tr>
                                    <td>{{ $row->item_code }}</td>
                                    <td>{{ $row->description }}</td>
                                    <td>{{ $row->quantity }}</td>
                                    <td>{{ _c($row->price) }}</td>
                                    <td>{{ _c($row->total) }}</td>
                                    <td>{{ $row->pv }}</td>
                                    <td>{{ $row->bv }}</td>
                                </tr>
                            @endforeach

                            @if ($order->shipping > 0)
                                <tr>
                                    <td colspan="4">Shipping</td>
                                    <td>{{ _c($order->shipping) }}</td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            @endif

                        </tbody>
                    </table>
                </div>

            @endif

            @if ($order->children->count() > 0)
                <h5>Child Orders</h5>

                <div class="table-responsive push">

                    <table class="table table-bordered table-striped table-sm bg-white">
                        <thead>
                            <tr>
                                <th style="width: 40px;">OrderID</th>
                                <th>NeoLifeID</th>
                                <th>Name</th>
                                <th>Total</th>
                                <th>ExigoID</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($order->children as $row)
                                <tr style="cursor:pointer" onclick="window.location = '/admin/order/{{ $row->id }}'">
                                    <td>{{ $row->reference }}</td>
                                    <td>{{ $row->neolife_id }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ _c($row->total) }}</td>
                                    <td>{{ $row->exigo_order_id }}</td>
                                    <td>{{ _badge($row->status) }}</td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            @endif

            <h5>History</h5>

            <ul class="timeline">

                @foreach ($order->logs as $row)
                    <li>
                        <a href="javascript:;">{{ $order->reference }}</a>
                        <span class="text-muted float-right">{{ _d($row->created_at) }}</span>
                        <p>{{ $row->log }}</p>
                    </li>
                @endforeach
            </ul>


        </div>
    </div>

@endsection
