@extends('layouts.app')

@section('content')

<div class="block">
    <div class="block-content bg-light">
        <h3 class="font-size-sm text-uppercase mb-0">{{ucfirst($source)}} Conversation between Mcommerce and {{$user->neolife_id}} | {{$user->phone}}</h3>
    </div>
    <div class="block-content" style="height:300px !important; overflow-y: scroll" id="chat">

        <div class="alert alert-default text-center">
            <i class="fa fa-ellipsis-h text-muted fa-2x"></i>
        </div>

        @foreach($activities->reverse() as $row)

        @if(\Illuminate\Support\Str::contains($row->properties, 'reply'))
        
        <div class="alert alert-danger ml-5">
            <strong>Mcommerce {{_d($row->created_at)}}</strong><br>
            {!! $row->description !!}
        </div>

        @else

        <div class="alert alert-primary mr-5">
            <strong>{{$user->neolife_id}} {{_d($row->created_at)}}</strong><br>
            {!! $row->description !!}
        </div>

        @endif

        @endforeach
        
    </div>
</div>

<div class="block">

    <div class="block-content bg-light">
        <h3 class="font-size-sm text-uppercase mb-0">Send a {{ucfirst($source)}} Message to {{$user->neolife_id}}</h3>
    </div>
    <div class="block-content">

       <form action="/admin/send-message" method="POST" role="form">

            {{csrf_field()}}

           <div class="form-group">
               <label>Message</label>
               <textarea name="message" class="form-control" rows="3" required></textarea>
               <input type="hidden" name="user_id" value="{{$user->id}}">
               <input type="hidden" name="source" value="{{$source}}">
           </div>
       
           <button type="submit" class="btn btn-primary">Send Now</button>
       
       </form>
        
    </div>
</div>

<script type="text/javascript">
  let element = document.getElementById('chat');
  element.scrollTop = element.scrollHeight;
</script>
@endsection