@extends('layouts.app')

@section('content')

<form method="GET" action="/admin/conversations/search">
        
    <div class="input-group mb-3">
        <input type="hidden" name="source" value="{{$source}}">
        <input type="text" class="form-control" placeholder="Search by Neolife ID" name="query" required>
        <div class="input-group-append">
            <button type="submit" class="input-group-text btn btn-primary">Search</button>
        </div>
    </div>
    
</form>


<div class="block">
    <div class="block-content bg-light">
        <h3 class="font-size-sm text-uppercase mb-0">Conversations</h3>
    </div>
    <div class="block-content">

        <div class="table-responsive">
            <table class="table table-borderless table-hover table-vcenter font-size-sm mb-0">
                <thead>
                    <tr>
                        <th>NeolifeID</th>
                        <th>Phone</th>
                        <th>Name</th>
                        <th>{{ucfirst($source)}}Messages</th>
                        <th>LastMessageDate</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($activities as $row)

                    <tr style="cursor:pointer" onclick="window.location = '/admin/conversation/{{$source}}/{{$row->user_id}}'">
                        <td>{{$row->neolife_id}}</td>
                        <td>{{$row->phone}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->messages_count}}</td>
                        <td><small>{{_d($row->last_message_date)}}</small></td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>

    </div>
</div>

@endsection