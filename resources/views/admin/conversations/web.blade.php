@extends('layouts.app')

@section('content')

<div id="web-messaging">

  <div class="alert alert-danger" v-if="error.length > 0">
    @{{error}}
  </div>

  <div class="input-group mb-3">
    <input type="text" class="form-control" placeholder="Enter Neolife ID eg 063-XXXXXXX" v-model="neolife_id" :disabled="loading">
    <div class="input-group-append">
      <button type="button" v-on:click="getUser" class="input-group-text btn btn-primary" v-if="!loading">Start Conversation</button>
      <button type="button" class="input-group-text btn btn-default" disabled v-else>Loading...</button>
    </div>
  </div>

  <div class="block" v-if="user && messages.length > 0">
    <div class="block-content bg-light">
      <h3 class="font-size-sm text-uppercase mb-0">Web Conversation between Mcommerce and @{{user.neolife_id}} @{{user.phone}}</h3>
    </div>
    <div class="block-content" style="height:300px !important; overflow-y: scroll" ref="chat">

      <div v-for="row in messages">

        <div v-if="row.type == 'reply'" class="alert alert-danger ml-5">
          <strong>Mcommerce</strong><br>
          <span v-html="row.text"></span>
        </div>

        <div v-else class="alert alert-primary mr-5">
          <strong>@{{user.neolife_id}}</strong><br>
          <span v-html="row.text"></span>
        </div>

      </div>
      
    </div>
  </div>

  <div class="block" v-if="user">

    <div class="block-content bg-light">
      <h3 class="font-size-sm text-uppercase mb-0">Message</h3>
    </div>
    <div class="block-content">

     <form action="#" method="POST" role="form">

      <div class="form-group">
       <label>Message</label>
       <textarea name="message" class="form-control" rows="3" v-model="message" placeholder="NEO BUY PRODUCT QTY, PRODUCT QTY ID {ID} DC {DC} BANK {BANK}" :disabled="loading"></textarea>
     </div>

     <button type="button" v-on:click="send" class="btn btn-primary" v-if="time < 1 && !loading">Send</button>
     <button type="button" class="btn btn-danger disabled" v-if="time > 0 && !loading" disabled>Send another message in @{{time}}</button>
    <button type="button" v-on:click="send" class="btn btn-primary" v-if="loading" :disabled="loading">Requesting...</button>
   </form>

 </div>

</div>

</div>

@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="/js/web.js"></script>
@endsection