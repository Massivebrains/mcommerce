<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Mcommerce - Login</title>

    <meta name="description" content="Mcommerce Login">
    <meta name="author" content="Olaiya Segun">
    <meta name="robots" content="noindex, nofollow">

    <link rel="shortcut icon" href="/media/favicons/favicon.png">
    <link rel="apple-touch-icon" href="/media/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" href="/media/favicons/favicon-192x192.png">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/main.min.css">
</head>

<body>

    <div id="page-container">


        <div class="bg-image" style="background-image: url('/media/various/bg.png');">
            <div class="bg-image-overlay py-5">
                <header class="container d-md-flex align-items-md-center justify-content-md-between py-4">
                    <div class="text-center text-md-left py-3">
                        <a class="h4 text-dark font-weight-600" href="">
                            <i class="far fa-fw fa-circle text-success mr-1"></i> Login
                        </a>
                    </div>

                </header>
            </div>
        </div>


        <div class="container py-4">
            <div class="row">

                <div class="col-lg-6 offset-lg-3">

                    <form action="/login" method="POST" role="form">

                        {{ csrf_field() }}

                        @include('partials._alert')

                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" name="username" class="form-control" placeholder="Exigo Username" value="{{ old('username') }}" required>
                            @if ($errors->has('username'))
                                <div class="invalid-feedback">{{ $errors->first('username') }}</div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Exigo Password" required>
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="">Country</label>
                            <select name="country" class="form-control" required>
                                <option value="NG">Nigeria</option>
                                <option value="GH">Ghana</option>
                                <option value="ZA">South Africa</option>
                                <option value="TG">Togo</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Login</button>

                    </form>

                </div>
            </div>
        </div>

    </div>

</body>

</html>
