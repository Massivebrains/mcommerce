<form method="POST" action="/admin/orders/search">

    {{ csrf_field() }}

    <div class="input-group mb-3">
        <input type="text" class="form-control" placeholder="Search with Exigo or Mcommerce Order ID. Click on Print to print directly if found or Search to view search results" name="query" required>
        <div class="input-group-append">
            <button type="submit" class="input-group-text btn btn-primary" name="submit" value="print">Print</button>
        </div>
        <div class="input-group-append">
            <button type="submit" class="input-group-text btn btn-primary" name="submit" value="details">Search</button>
        </div>
    </div>

</form>
