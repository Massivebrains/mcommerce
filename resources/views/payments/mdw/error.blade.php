<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<title>Mcommerce Payments</title>
</head>
<body>


	<div class="container">

		<div class="jumbotron m-5 p-t-5">
			<h1 class="display-4 text-danger">Error</h1>
			<p class="lead">{{$error}}</p>
		</div>

	</div>
</body>
</html>