<!DOCTYPE html>
<html>

<body onload="document.forms['form'].submit()">

<form action="{{$url}}" method="POST" name="form">
	<input type="hidden" name="orderid" value="{{$order->reference}}">
	<input type="hidden" name="customerid" value="{{$order->user->neolife_id}}">
	<input type="hidden" name="mobilephone" value="{{$order->user->phone}}">
	<input type="hidden" name="amount" value="{{$order->total}}">
	<input type="hidden" name="returnurl" value="{{url('/payments/mdw/return-url')}}">
	<input type="hidden" name="callbackurl" value="{{url('/payments/mdw/callback-url')}}">
	<input type="hidden" name="country" value="{{$order->user->country_code}}">
	<input type="hidden" name="currencycode" value="{{$currency}}">
	<input type="hidden" name="provider" value="{{$order->payment_provider->provider ?? 'Paystack'}}">
	<input type="hidden" name="description" value="{{$description}}">
</form>

</body>
</html>