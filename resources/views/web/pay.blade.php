@extends('layouts.web')

@section('content')

<div id="app">

    <div class="top" v-if="env == 'local'">
        <h5>
            [TEST MODE] This is a test payment page. | 
            <a href="javascript:;" class="btn-link" v-on:click="simulate" style="color:#fff;">
                @{{simulate_text}}
            </a> 
        </h5>

    </div>

    <div class="top-live" v-if="env != 'local'"></div>

    <main class="main">

        <div class="modal-dialog">

            <div class="modal-content">
                <div class="mode">
                    <h1 class="mode-title">NEOLIFE INTERNATIONAL</h1>  
                    <p>Payment Service by Ogaranya</p>                    
                </div>

                <div class="details">
                    <h5 class="details-title">{{$payment->phone}} (NGN {{_c($payment->amount)}}) Order ID: {{$payment->reference}}</h5>
                </div>

                <div class="modal-body">

                    @foreach($providers as $row)

                    <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                            <p class="lead">{{$row->name}}</p>
                            <p class="ussd">{{$row->formatMessage($payment)}}</p>
                        </div>
                    </div>

                    @endforeach

                </div>
                <div class="modal-footer">
                    <span><i class="fa fa-spinner fa-spin" v-if="!paid"></i> Waiting for your payment...</span>
                    <button type="button" class="btn btn-link" v-on:click="cancel" onclick="">
                        <i class="fa fa fa-chevron-circle-left"></i> Back to merchant site
                    </button>
                </div>
            </div>
        </div>

    </main>

    <div class="bottom-live" v-if="env != 'local' "></div>

</div>
@endsection

@section('scripts')

<script type="text/javascript">

    var app = new Vue({

        el: '#app',

        data: {

            env: '{{env('APP_ENV')}}',
            paid: false,
            interval: null,
            redirect_url: '{{$payment->return_url}}',
            simulate_text: 'Click here to Simulate Payment'
        },

        created(){

            this.requery()

            this.interval = setInterval(() => {

                this.requery()

            }, 1000 * 5)
        },

        watch: {

            paid: function (newPaid, oldPaid) {

                if(newPaid == true)
                    window.location = this.redirect_url
            }
        },

        methods: {

            simulate(){

                this.simulate_text = 'Simulating Payment...'
                this.requery('test')

            },

            cancel(){

                window.location = this.redirect_url
            },

            requery(simulate = ''){

                fetch(`/web/requery/{{$payment->id}}/${simulate}`).then(resp => resp.json()).then(response => {

                    if(response.status == true){

                        clearInterval(this.interval)
                        this.redirect_url = response.data
                        this.paid = true
                    }
                })
            }
        }
    })

    document.onkeydown = function(e) {

        if (e.ctrlKey &&  (e.keyCode === 67 ||  e.keyCode === 86 ||  e.keyCode === 85 ||  e.keyCode === 117)) {

            return false;

        } else {

            return true;
        }
    }
</script>

@endsection