<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <title>Ogaranya Payments</title>
</head>
<body>
  

  <div class="container m-5 p-t-5">

    <h1 class="text-muted">Test Web Pay</h1>

    <form action="/web/pay" method="POST" role="form">

      <div class="row">

        <div class="form-group col-md-6">
          <label>Order ID</label>
          <input type="text" class="form-control" name="order_id" placeholder="Order ID" required>
        </div>

        <div class="form-group col-md-6">
          <label>Phone</label>
          <input type="text" class="form-control" name="phone" placeholder="Phone" required>
        </div>

        <div class="form-group col-md-6">
          <label>Amount</label>
          <input type="number" class="form-control" name="amount" placeholder="Amount" required>
        </div>

        <div class="form-group col-md-6">
          <label>Callback URL</label>
          <input type="url" class="form-control" name="callback_url" placeholder="Callback URL" value="https://google.com" required>
        </div>

        <div class="form-group col-md-6">
          <label>Return URL</label>
          <input type="url" class="form-control" name="return_url" placeholder="Callback URL" value="https://google.com" required>
        </div>

        <div class="form-group col-md-6">
          <label>Private Key</label>
          <input type="text" class="form-control" name="private_key" placeholder="Private Key" value="I5nOfIJXbugQ0mgpPHUwkGz7z2wBxTolv22" required>
        </div>

      </div>
      
      <button type="submit" class="btn btn-primary">Submit</button>

    </form>
  </div>
</body>
</html>