<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Jobs\RefreshUserFromExigo;
use App\Services\Exigo;
use Carbon\Carbon;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];
    protected $dates    = ['created_at', 'updated_at', 'expire_at'];
    protected $casts    = ['expire_at' => 'datetime'];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function grouped_orders()
    {
        return $this->hasMany('App\GroupedOrder');
    }

    public static function getUserFromExigo($field = 'neolife_id', $value = '', $message = '')
    {
        $user = User::where([$field => $value])->whereStatus('active')->first();

        if ($user && $user->expire_at != null) {
            if ($user->expire_at > Carbon::now()) {
                return $user;
            }
        }


        $response = Exigo::get('api/customers/find/'.$value);

        if ($response->status == false) {
            return $response->data;
        }

        $exigo_field = $field == 'neolife_id' ? 'NeolifeId' : 'Phone';

        $user = collect($response->data)->firstWhere($exigo_field, $value);

        if (!$user) {
            if (count($response->data) > 0) {
                return $response->data[0]->Message;
            }

            return __('errors.invalid_customer');
        }

        $eloquentUser = User::updateOrCreate(['phone' => $user->Phone], [

            'customer_id'   => $user->Id,
            'neolife_id'    => $user->NeolifeId,
            'name'          => $user->Name,
            'phone'         => $user->Phone,
            'country'       => $user->CountryDescription,
            'country_no'    => $user->CountryNo,
            'country_code'  => $user->CountryCode,
            'currency_code' => $user->CountryCode[0],
            'status'        => $user->Status,
            'expire_at'     => date('Y-m-d', strtotime($user->ExpiryDate))
        ]);

        if ($user->Status != 'active') {
            if (self::checkIfCustomerIsBuyingRenewal($message)) {
                return $eloquentUser;
            }

            return $user->Message;
        }

        if ($eloquentUser->expire_at != null && $eloquentUser->expire_at < Carbon::now()) {
            return 'Your Neolife subscription has expired, kindly renew to continue to make the world a healthier and happier place.';
        }

        return $eloquentUser;
    }

    public static function checkIfCustomerIsBuyingRenewal($message)
    {
        if (Str::startsWith($message, 'NEO BUY')) {
            $products = _find_between($message, 'NEO BUY', 'ID');

            if (Str::contains($products, '9999')) {
                return true;
            }
        }

        if (Str::startsWith($message, 'NEO GROUP')) {
            $products = _find_between($message, 'NEO GROUP', 'ID');

            if (Str::contains($products, '9999')) {
                return true;
            }
        }

        return false;
    }

    public static function getUserFromMessage($message)
    {
        $neolife_id         = _find_between($message, 'ID', 'DC');
        $location_id        = _find_between($message, 'DC', 'BANK');
        $location           = Location::whereCode($location_id)->first();

        if (!$location) {
            return null;
        }

        if (strlen($neolife_id) == 6) {
            $neolife_id = "0{$neolife_id}";
        }

        if (strlen($neolife_id) == 7) {
            $neolife_id = "{$location->country_no}-{$neolife_id}";
        }

        return self::where(['neolife_id' => $neolife_id])->first();
    }

    public static function kioskUsers()
    {
        return collect([
            ['username' => 'gbagada', 'password' => 'YjSqytRO0TMOO'],
            ['username' => 'abeokuta', 'password' => 'A5XTVzpzg'],
        ]);
    }
}
