<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelScopes;

class OrderDetail extends Model
{
	use ModelScopes;
	
    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];
    protected $appends  = ['product_name'];

    public function getProductNameAttribute()
    {
        if($this->description){

            $names = explode(' ', $this->description);

            if(strlen($names[0]) < 5 && count($names) > 1)
                return $names[0].' '.$names[1];

            return $names[0];
        }

        return '';
    }

    public function order()
    {
    	return $this->belongsTo('App\Order')->withDefault(function(){

    		return new Order();
    	});
    }
}
