<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelScopes;
use Illuminate\Support\Str;

class Order extends Model
{
    use ModelScopes;

    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];
    protected $appends  = ['amount'];
    protected $casts    = [

        'created_at'    => 'datetime',
        'updated_at'    => 'datetime',
        'paid_at'       => 'datetime'
    ];



    public function getAmountAttribute()
    {
        return number_format($this->total, 2);
    }

    public function user()
    {
        return $this->belongsTo('App\User')->withDefault(function () {
            return new User();
        });
    }

    public function location()
    {
        return $this->belongsTo('App\Location')->withDefault(function () {
            return new Location();
        });
    }

    public function payment_provider()
    {
        return $this->belongsTo('App\PaymentProvider')->withDefault(function () {
            return new PaymentProvider();
        });
    }

    public function order_details()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function logs()
    {
        return $this->hasMany('App\Log');
    }

    public function master()
    {
        return $this->belongsTo('App\Order', 'group_order_id');
    }

    public function children()
    {
        return $this->hasMany('App\Order', 'group_order_id');
    }

    public static function search($search = null)
    {
        $query = self::where('status', '!=', 'cancelled');

        if (strlen($search) === 10) {
            $query->where('reference', $search);
            return $query->paginate(50);
        }

        if (Str::contains($search, '-')) {
            $query->where('neolife_id', $search);
        } else {
            $query->where('exigo_order_id', $search);
            $query->orWhere('reference', $search);
            $query->orWhere('transaction_reference', $search);
        }

        return $query->paginate(50);
    }

    public function logHttpRequest($request = [], $headers = [], $response = []) : void
    {
        HttpRequestLog::create([
            'scope' => 'order',
            'scope_id' => $this->id,
            'headers' => $headers,
            'request' => $request,
            'response' => $response
        ]);
    }
}
