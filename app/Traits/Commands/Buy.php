<?php

namespace App\Traits\Commands;

use App\Location;
use App\Order;
use App\OrderDetail;
use App\PaymentProvider;
use App\Services\Exigo;
use App\User;
use Illuminate\Support\Str;

trait Buy
{
    public $user;
    public $message;
    public $order;

    public function buy(User $user, $message, $source, $meta = [])
    {
        $this->user = $user;
        $this->message = $message;
        $this->source = $source;

        if (Str::startsWith($this->message, 'NEO BUY')) {
            $incomplete_orders = Order::where(['user_id' => $this->user->id, 'status' => 'incomplete', 'type' => 'single'])->get();

            foreach ($incomplete_orders as $row) {
                $row->update(['status' => 'cancelled']);
            }
        }

        $this->order = Order::whereUserId($this->user->id)->single()->incomplete()->first();

        if (!$this->order) {
            return $this->calculateOrder($this->user, $this->message);
        } else {
            if ($this->message == '1') {
                _order_log($this->order, 'Confirmed Order Details.');

                $this->order->update([

                    'status' => 'pending',
                    'reference' => _order_reference(),
                    'payment_reference' => _payment_reference(),
                ]);

                return $this->formatPaymentMessage($this->order);
            }
        }

        return __('errors.invalid_format_no_incomplete');
    }

    private function calculateOrder()
    {
        $payload = $this->getOrderPayload($this->user, $this->message);

        if (!is_array($payload)) {
            return $payload;
        }

        $exigo_response = Exigo::post('order/calculate', $payload);

        if ($exigo_response->status == false) {
            return (string) $exigo_response->data;
        }

        $exigo_order = $exigo_response->data;

        $this->order = Order::create([

            'user_id' => $this->user->id,
            'neolife_id' => $payload['neolifeid'],
            'name' => $payload['name'],
            'location_id' => $payload['location_id'],
            'payment_provider_id' => $payload['payment_provider_id'],
            'shipping' => $exigo_order->ShippingTotal,
            'total' => $exigo_order->Other10Total,
            'tax' => $exigo_order->OrderTax,
            'pv' => $exigo_order->BusinessVolumeTotal,
            'bv' => $exigo_order->CommissionableVolumeTotal,
            'country_code' => $this->user->country_code,
            'source' => $this->source,
        ]);

        $this->order->logHttpRequest($payload, [], $exigo_response);

        foreach ($exigo_order->Details as $row) {
            OrderDetail::create([

                'order_id' => $this->order->id,
                'item_code' => $row->ItemCode,
                'description' => $row->Description,
                'quantity' => $row->Quantity,
                'price' => $row->Other10Each,
                'total' => $row->Other10,
                'tax' => $row->Tax,
                'pv' => $row->BusinessVolume,
                'bv' => $row->CommissionableVolume,
            ]);
        }

        _order_log($this->order, 'Created');

        return $this->formatOrderDetails();
    }

    private function getOrderPayload()
    {
        foreach (['NEO BUY', 'ID', 'DC', 'BANK'] as $row) {
            if (!Str::contains($this->message, $row)) {
                return __('errors.invalid_format_missing_code');
            }
        }

        $products = _find_between($this->message, 'NEO BUY', 'ID');
        $neolife_id = _find_between($this->message, 'ID', 'DC');
        $location_id = _find_between($this->message, 'DC', 'BANK');
        $bank_id = trim(Str::after($this->message, 'BANK'));

        $location = Location::whereCode($location_id)->first();

        if (!$location) {
            return __('errors.invalid_location');
        }

        if ($location->status == 'inactive') {
            return __('errors.inactive_location');
        }

        if (strlen($neolife_id) < 6) {
            return __('errors.invalid_neolife_id');
        }

        if (strlen($neolife_id) == 6) {
            $neolife_id = "0{$neolife_id}";
        }

        if (strlen($neolife_id) == 7) {
            $neolife_id = "{$location->country_no}-{$neolife_id}";
        }

        if (strlen($neolife_id) != 11) {
            return __('errors.invalid_international_neolife_id');
        }

        $name = $this->user->name;

        if ($this->user->neolife_id != $neolife_id) {
            $user = User::getUserFromExigo('neolife_id', $neolife_id, $this->message);

            if ($user instanceof User) {
                $name = $user->name;
            } else {
                return $user;
            }
        }

        $payment_provider = PaymentProvider::whereCode($bank_id)->first();

        if (!$payment_provider) {
            return __('errors.invalid_bank');
        }

        if ($payment_provider->status == 'inactive') {
            return __('errors.inactive_bank');
        }

        if ($location->country_code != $payment_provider->country_code) {
            return __('errors.invalid_bank');
        }

        $products = explode(',', $products);
        $items = [];

        foreach ($products as $row) {
            $item = explode(' ', trim($row));

            if (count($item) != 2) {
                return __('errors.invalid_products_format');
            }

            $items[] = ['itemcode' => $item[0], 'quantity' => $item[1]];
        }

        $items = $this->toCases($items, $this->user->country_code);

        return [

            'password' => 'SecretNeolifePassword',
            'neolifeid' => $neolife_id,
            'name' => $name,
            'phone' => $this->user->phone,
            'warehouseid' => $location->warehouse_id,
            'shipmethodid' => $location->ship_method,
            'products' => $items,
            'location_id' => $location->id,
            'payment_provider_id' => $payment_provider->id,
        ];
    }

    private function formatOrderDetails()
    {
        $this->message = 'Find Details of your Order Below:' . PHP_EOL . PHP_EOL;
        $this->message .= 'ID: ' . $this->order->neolife_id . PHP_EOL;
        $this->message .= 'Name: ' . $this->order->name . PHP_EOL;
        $this->message .= 'Location: ' . $this->order->location->name . PHP_EOL;
        $this->message .= 'Payment Option: ' . $this->order->payment_provider->name . PHP_EOL;

        if ($this->order->shipping > 0) {
            $this->message .= 'Shipping: ' . number_format($this->order->shipping, 2) . PHP_EOL;
        }

        $this->message .= 'Currency: ' . $this->order->location->currency . PHP_EOL . PHP_EOL;

        foreach ($this->order->order_details as $row) {
            $price = number_format($row->price);
            $total = number_format($row->total);

            if ($row->item_code == 'Stamp Duty') {
                $this->message .= "Stamp Duty = {$total}" . PHP_EOL;
                continue;
            }

            if ($row->total < 1) {
                $this->message .= "{$row->item_code} {$row->product_name} = {$row->quantity}" . PHP_EOL;
                continue;
            }

            $this->message .= "{$row->item_code} {$row->product_name} = {$price} X {$row->quantity} => {$total}" . PHP_EOL;
        }

        $this->message .= "Total = {$this->order->amount}" . PHP_EOL . PHP_EOL;
        $this->message .= "Send 1 to complete your order. To make changes please resend correct order details. Do tell others about this platform, Thanks.";

        return $this->message;
    }

    private function formatPaymentMessage()
    {
        $total = number_format($this->order->total);

        $this->message = "Your order ID is {$this->order->reference}, please write it down. Order Total is {$this->order->amount}" . PHP_EOL . PHP_EOL;

        $this->message .= $this->order->payment_provider->formatMessage($this->order) . PHP_EOL . PHP_EOL;

        $this->message .= "For inquiries please call 08131771532, 08131771533 or 08131771534.";

        return $this->message;
    }
}
