<?php

namespace App\Traits\Commands;

use App\Location;
use App\Order;
use App\OrderDetail;
use App\PaymentProvider;
use App\Services\Exigo;
use App\User;
use Illuminate\Support\Str;

trait Group
{
    public $master;
    public $user;
    public $message;
    public $source;

    public function group(User $user, $message, $source, $meta = [])
    {
        $this->user = $user;
        $this->message = $message;
        $this->source = $source;
        $this->master = Order::whereUserId($this->user->id)->master()->incomplete()->first();

        if ($this->message == 'CANCEL') {
            return $this->cancelGroupOrders();
        }

        if (!$this->master) {
            $this->master = Order::create([

                'user_id' => $this->user->id,
                'country_code' => $this->user->country_code,
                'reference' => _order_reference(),
                'payment_reference' => _payment_reference(),
                'type' => 'master',
                'source' => $this->source,
            ]);
        }

        if ($this->message == '2') {
            if ($this->master->children->count() < 1) {
                return __('errors.no_children_in_group_order');
            }

            _order_log($this->master, 'Confirmed Group Order Details.');

            $this->master->update(['status' => 'pending']);

            $this->master->children->each(function ($row) {
                $row->update(['status' => 'pending', 'reference' => _order_reference()]);
            });

            return $this->formatOrderPaymentMessage();
        }

        return $this->processChildOrder();
    }

    private function processChildOrder()
    {
        $payload = $this->getChildOrderPayload();

        if (!is_array($payload)) {
            return $payload;
        }

        $exigo_response = Exigo::post('order/calculate', $payload);

        if ($exigo_response->status == false) {
            return (string) $exigo_response->data;
        }

        $exigo_order = $exigo_response->data;

        $order = Order::create([

            'user_id' => $this->user->id,
            'neolife_id' => $payload['neolifeid'],
            'name' => $payload['name'],
            'location_id' => $payload['location_id'],
            'payment_provider_id' => $payload['payment_provider_id'],
            'group_order_id' => $this->master->id,
            'shipping' => $exigo_order->ShippingTotal,
            'total' => $exigo_order->Other10Total,
            'tax' => $exigo_order->OrderTax,
            'pv' => $exigo_order->BusinessVolumeTotal,
            'bv' => $exigo_order->CommissionableVolumeTotal,
            'country_code' => $this->user->country_code,
            'type' => 'child',
            'source' => $this->source,
        ]);

        $order->logHttpRequest($payload, [], $exigo_response);

        foreach ($exigo_order->Details as $row) {
            OrderDetail::create([

                'order_id' => $order->id,
                'item_code' => $row->ItemCode,
                'description' => $row->Description,
                'quantity' => $row->Quantity,
                'price' => $row->Other10Each,
                'total' => $row->Other10,
                'tax' => $row->Tax,
                'pv' => $row->BusinessVolume,
                'bv' => $row->CommissionableVolume,
            ]);
        }

        $this->master = Order::find($this->master->id);

        $this->master->update([

            'payment_provider_id' => $order->payment_provider_id,
            'location_id' => $order->location_id,
            'total' => $this->master->children->sum('total'),
            'pv' => $this->master->children->sum('pv'),
            'bv' => $this->master->children->sum('bv'),

        ]);

        _order_log($order, 'Created');

        return $this->formatChildOrderDetails($order);
    }

    private function getChildOrderPayload()
    {
        foreach (['NEO GROUP', 'ID', 'DC', 'BANK'] as $row) {
            if (!Str::contains($this->message, $row)) {
                return __('errors.invalid_group_format');
            }
        }

        $products = _find_between($this->message, 'NEO GROUP', 'ID');
        $neolife_id = _find_between($this->message, 'ID', 'DC');
        $location_id = _find_between($this->message, 'DC', 'BANK');
        $bank_id = trim(Str::after($this->message, 'BANK'));

        $location = Location::whereCode($location_id)->first();

        if (!$location) {
            return __('errors.invalid_location');
        }

        if ($location->status == 'inactive') {
            return __('errors.inactive_location');
        }

        if (strlen($neolife_id) < 6) {
            return __('errors.invalid_neolife_id');
        }

        if (strlen($neolife_id) == 6) {
            $neolife_id = "0{$neolife_id}";
        }

        if (strlen($neolife_id) == 7) {
            $neolife_id = "{$location->country_no}-{$neolife_id}";
        }

        if (strlen($neolife_id) != 11) {
            return __('errors.invalid_international_neolife_id');
        }

        $name = $this->user->name;

        if ($this->user->neolife_id != $neolife_id) {
            $user = User::getUserFromExigo('neolife_id', $neolife_id, $this->message);

            if ($user instanceof User) {
                $name = $user->name;
            } else {
                return $user;
            }
        }

        $payment_provider = PaymentProvider::whereCode($bank_id)->first();

        if (!$payment_provider) {
            return __('errors.invalid_bank');
        }

        if ($payment_provider->status == 'inactive') {
            return __('errors.inactive_bank');
        }

        if ($location->country_code != $payment_provider->country_code) {
            return __('errors.invalid_bank');
        }

        foreach ($this->master->children as $row) {
            if ($row->location->country_code != $location->country_code) {
                return __('errors.invalid_multiple_countries');
            }

            if ($row->payment_provider_id != $payment_provider->id) {
                return __('errors.invalid_multiple_banks');
            }
        }

        $products = explode(',', $products);
        $items = [];

        foreach ($products as $row) {
            $item = explode(' ', trim($row));

            if (count($item) != 2) {
                return __('errors.invalid_products_format');
            }

            $items[] = ['itemcode' => $item[0], 'quantity' => $item[1]];
        }

        $items = $this->toCases($items, $this->user->country_code);

        return [

            'password' => 'SecretNeolifePassword',
            'neolifeid' => $neolife_id,
            'name' => $name,
            'phone' => $this->user->phone,
            'warehouseid' => $location->warehouse_id,
            'shipmethodid' => $location->ship_method,
            'products' => $items,
            'location_id' => $location->id,
            'payment_provider_id' => $payment_provider->id,
        ];
    }

    private function formatChildOrderDetails($order)
    {
        $child_total = number_format($order->total);
        $child_orders = Order::whereGroupOrderId($this->master->id)->count();
        $master_total = number_format($this->master->total);

        $message = 'Find Details of your Child Order below:' . PHP_EOL . PHP_EOL;
        $message .= 'Group Order: ' . $this->master->reference . PHP_EOL;
        $message .= 'ID: ' . $order->neolife_id . PHP_EOL;
        $message .= 'Name: ' . $order->name . PHP_EOL;
        $message .= 'Location: ' . $order->location->name . PHP_EOL;
        $message .= 'Payment Option: ' . $order->payment_provider->name . PHP_EOL;

        if ($order->shipping > 0) {
            $message .= 'Shipping: ' . number_format($order->shipping, 2) . PHP_EOL;
        }

        $message .= 'Currency: ' . $order->location->currency . PHP_EOL . PHP_EOL;

        foreach ($order->order_details as $row) {
            $price = number_format($row->price);
            $total = number_format($row->total);

            if ($row->item_code == 'Stamp Duty') {
                $message .= "Stamp Duty = {$total}" . PHP_EOL;
                continue;
            }

            if ($row->total < 1) {
                $message .= "{$row->item_code} {$row->product_name} = {$row->quantity}" . PHP_EOL;
                continue;
            }

            $message .= "{$row->item_code} {$row->product_name} = {$price} X {$row->quantity} => {$total}" . PHP_EOL;
        }

        $message .= PHP_EOL;
        $message .= "Child Total = {$child_total}" . PHP_EOL;
        $message .= "Child Orders = {$child_orders}" . PHP_EOL;
        $message .= "Group Total = {$master_total}" . PHP_EOL . PHP_EOL;
        $message .= "Send 2 to review your group order or send another group order to continue.";

        return $message;
    }

    private function formatOrderPaymentMessage()
    {
        $message = "Your Group Order ID is {$this->master->reference}, your child orders summary is as follows." . PHP_EOL . PHP_EOL;

        foreach ($this->master->children as $row) {
            $name = '';
            $names = explode(' ', $row->name);

            if (count($names) > 0) {
                $name = '(' . $names[0] . ')';
            }

            $message .= "{$row->neolife_id}{$name} => PV:{$row->pv} Total:{$row->amount}" . PHP_EOL . PHP_EOL;
        }

        $message .= PHP_EOL . "Group Order Total is {$this->master->amount}" . PHP_EOL . PHP_EOL;
        $message .= $this->master->payment_provider->formatMessage($this->master) . PHP_EOL . PHP_EOL;
        $message .= "For inquiries please contact any of our closest offices. Thank you for your patronage.'";

        return $message;
    }

    private function cancelGroupOrders()
    {
        $masters = Order::whereUserId($this->user->id)->incomplete()->get();

        foreach ($masters as $row) {
            $row->children->each(function ($order) {
                $order->update(['status' => 'cancelled']);
            });

            $row->update(['status' => 'cancelled']);
        }

        return __('messages.group_orders_cancelled');
    }
}
