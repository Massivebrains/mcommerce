<?php

namespace App\Traits\Commands;

use Illuminate\Support\Str;
use Validator;
use App\Services\Exigo;
use App\Rules\ExigoCustomer;

trait Join
{
	public function join_telegram()
	{		
		return __('errors.already_joined_telegram');
	}
}