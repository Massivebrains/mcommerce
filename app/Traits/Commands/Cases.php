<?php 

namespace App\Traits\Commands;

trait Cases 
{

	private function toCases($products = [], $country_code = 'NG')
	{
		$items = [];

		foreach($products as $row){

			$item_code 	= $row['itemcode'];
			$quantity 	= $row['quantity'];

			if($quantity <= 0)
				continue;
			
			if($quantity >= 6 && $item_code != '2741'){

				$cases_unit = 6;

				if($country_code == 'NG' && $item_code == '2130')
				$cases_unit = 8;

				$cases 		= ($quantity - ($quantity % $cases_unit)) / $cases_unit;
				$singles 	= $quantity - ($cases * $cases_unit); 

				if($cases > 0){

					$items[] = ['itemcode' => $item_code.'C', 'quantity' => $cases];
				}

				if($singles > 0){

					$items[] = ['itemcode' => $item_code, 'quantity' => $singles];
				}

			}else{

				$items[] = ['itemcode' => $item_code, 'quantity' => $quantity];
			}

		}

		return $items;
	}
}