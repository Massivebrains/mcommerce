<?php 

namespace App\Traits;

use App\Jobs\ProcessOrderOnExigo;
use App\Order;
use App\Traits\TextResponse;

trait Pay
{
	use TextResponse;

	public function payForOrder($order, $transaction_reference)
	{
		if($order->type == 'master')
			return $this->payForGroupOrder($order, $transaction_reference);

		_order_log($order, 'Attemped to update order payment info');

		$order = Order::where(['id' => $order->id, 'status' => 'pending'])->first();

		if(!$order){

			_order_log($order, 'Attempted to pay for already PAID order');

			return false;
		}

		if(!$transaction_reference){

			_order_log($order, 'Cannot update order with invalid transaction reference');

			return false;
		}

		$order->update([

			'transaction_reference' 	=> $transaction_reference,
			'status'					=> 'paid',
			'paid_at'               	=> date('Y-m-d H:i:s')

		]);

		_order_log($order, 'Payment successful');

		$message = 'Payment was Successful. Order '.$order->reference.' has been successfully Completed.';

		$this->reply($message, $order->source, $order->user);
		$this->reply(_get_info(), $order->source, $order->user);
		
		if($order->type == 'single')
			ProcessOrderOnExigo::dispatch($order);

		return true;
	}

	public function payForGroupOrder($master, $transaction_reference)
	{	
		_order_log($master, 'Attemped to update group order payment info');

		if($master->status != 'pending'){

			_order_log($master, 'Attempted to pay for already PAID group order');

			return false;
		}

		if(!$transaction_reference){

			_order_log($master, 'Cannot update group order with invalid transaction reference :: '.$transaction_reference);

			return false;
		}

		$master->update([

			'transaction_reference'   => $transaction_reference,
			'status'                  => 'paid',
			'paid_at'                 => date('Y-m-d H:i:s')
		]);

		$message = 'Payment was Successful. Group Order '.$master->reference.' has been successfully Completed.';
		$this->reply($message, $master->source, $master->user);
		
		$master->children->each(function($order) use ($transaction_reference) {

			$order = Order::where(['id' => $order->id, 'status' => 'pending'])->first();
			
			if($order->status == 'paid'){

				_order_log($order, 'Child order already paid while attempting to pay from group order funds. no changes was made to this specific child order.');
				
			}else{

				_order_log($order, 'Attemped to update child order payment info');

				$order->update([

					'transaction_reference'   => $transaction_reference,
					'status'                  => 'paid',
					'paid_at'                 => date('Y-m-d H:i:s')

				]);

				_order_log($order, 'Payment successful for child order via group order');

				ProcessOrderOnExigo::dispatch($order);
			}	

		});

		return true;

	}
}