<?php 

namespace App\Traits;
use App\User;
use App\Traits\Responders\WhatsappResponder;
use App\Traits\Responders\TelegramResponder;

trait TextResponse
{
	use WhatsappResponder, TelegramResponder;

	public function reply($text = '', $source = '', $user = null, $telegram_id = null)
	{
		if($user && $user instanceof User){

			if($source == 'whatsapp')
				$this->whatsappReply($user->phone, $text);

			if($source == 'telegram')
				$this->telegramReply($user->telegram_id, $text);
		}

		if($source == 'telegram' && $telegram_id){

			$this->telegramReply($telegram_id, $text);
		}
		
		return response($text, 200)->header('Content-Type', 'text/plain');
	}
}