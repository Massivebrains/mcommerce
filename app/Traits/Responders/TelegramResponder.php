<?php 

namespace App\Traits\Responders;

use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Exception\TelegramException;
use Illuminate\Support\Str;

trait TelegramResponder
{    
    private $bot_api_key    = '295972786:AAHQahIa3fFI0q_IK1i8RFP-3N8pQpYpV_4';
    private $hook_url       = 'https://mcommerce.neolifeafrica.info/api/v1/telegram';
    //private $hook_url       = 'https://0150be742ba4.ngrok.io/api/v1/telegram';
    private $bot_username   = 'NeolifeBot';

    public function telegramReply($chat_id, $text)
    {
        try{

            $telegram = new Telegram($this->bot_api_key, $this->bot_username);

            if(env('APP_ENV', 'local') == 'staging')
                $telegram = new Telegram('1126627477:AAEzWRbUH4q1qQmnfgoZRU1j3Xu4NmS8jt8', 'NeolifeSandboxBot');

            $message = \Longman\TelegramBot\Request::sendMessage([

                'chat_id'       => $chat_id,
                'text'          => html_entity_decode($this->telegramReplaceUSSDStringWithLink($text)),
                'parse_mode'    => 'HTML'
            ]);

            return $text;

        }catch(TelegramException $e){

            \Log::info($e->getMessage());
            return $e->getMessage();
        }
    }

    public function telegramSetWebHook($url = null)
    {
        try{

            $telegram = new Telegram($this->bot_api_key, $this->bot_username);
            //$telegram = new Telegram('1126627477:AAEzWRbUH4q1qQmnfgoZRU1j3Xu4NmS8jt8', 'NeolifeSandboxBot');

            $url = $url ?? $this->hook_url;

            $result = $telegram->setWebhook($url);

            dd(['result' => $result, 'url' => $url]);

        }catch(TelegramException $e){

            dd($e->getMessage());
        }
    }

    public function telegramGetWebHook()
    {
        try{

            $telegram = new Telegram($this->bot_api_key, $this->bot_username);

            return $telegram->getWebhook($this->hook_url);

        }catch(TelegramException $e){

            dd($e->getMessage());
        }
    }

    public function telegramReplaceUSSDStringWithLink($message)
    {
        $code = _find_between($message, '*', '#');

        if(strlen($code) < 1)
            return $message;

        $ussd = "*$code#";
        $link = url('/t?code='.$ussd);

        return Str::replaceLast($ussd, "<a href='$link'>$ussd</a>", $message);
    }
}