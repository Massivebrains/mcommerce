<?php

namespace App\Traits\Responders;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use App\User;

trait WhatsappResponder
{
    private $endpoint_9032078628 	= 'https://api.positus.global/v2/whatsapp/numbers/c28e89ec-2a29-40ba-a7cf-ce272710435b/messages';
    private $endpoint_9037648324 	= 'https://api.positus.global/v2/whatsapp/numbers/adcabff9-250f-4227-8aa0-b74202bac06b/messages';
    private $token 					= 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOTdhOTNmNmIwYTIwZjBmNDM0ZmMxMzRkZjJmOGNlYzk4NzcwMThkOGE4NGE3N2U2YThjYTY5MzJhZTExMmY3YmZjNzRiYWFlMzgwYjhhNzIiLCJpYXQiOjE2MjY3NDExNTguNzQ5MiwibmJmIjoxNjI2NzQxMTU4Ljc0OTIwOSwiZXhwIjoxNjU4Mjc3MTU4Ljc0NTk2NSwic3ViIjoiMTA5OCIsInNjb3BlcyI6W119.M6jIHHHd8SwPKBEn8I1TY_2-D2JQCvxuPOjOd8tOyOhN3UuKvYjqtyMJd1ZVRe3f2BTgbJ5HQnbcC9jY7zdPlDdCN1_00ZYKUVh6S980q_O6FYx3dZiPjQXO0vtjbbw1DHoUsNIxj959WLn4IzLjPuERE8Wn8TfrjUM1JnrXAijbDXXw7OZcTGX60m3pMo5rd_lu1IcE22_Oy3b2bmETuhKS_2E7uGoSaqencGQVRAjaewfOLtOjrPwi8iXLSg_0LvbiadhSYrHDqa42qCyCMu9FPmxEbt_59LUvg9c4WCBl8RmH_e7RCUgNBYvrKebnxqX6m8X9WQMD7PC4WI3pqfYbvaAQ3OqfJQ4QchOKy7TR8atDooRPXhBZ69tnhb-x_l-CLIQ-386K9zGCxUFoByqYg9nPOOFu-bGfQfHyvNIaBufFQv3OHZXytGXHtalMOmMNF8I240Cpuwh8m8zTicoTTMwXCHN-yWmB2-oNpt3d81YC3buyExMYVpDJqoIRqNFqacYfHzmo3SP5Y4qziR_I3Uu6vaMpa00P5VRR_jaNUwdCRWcvSVHcwm8hVXuYxI-8K_CdAAt6O-CqP5QPbdzjR2WTFGIhaYv2au9BzqRbQf5ZizIT-UpCJD5xV6TghF3edQROMx9MvwggJ1J1NbeoNTEEjnz5gBDzzvRtQNc';

    public function whatsappReply($phone, $text = '')
    {
        try {
            $payload = [

                'to'	=> '+'.$phone,
                'type'	=> 'text',
                'text'	=> [

                    'body'	=> $text
                ]
            ];

            $response = (new Client())->post($this->endpoint_9032078628, [

                'headers' => [

                    'Authorization'	=> "Bearer $this->token",
                    'Accept' 		=> 'application/json'
                ],

                RequestOptions::JSON => $payload
            ]);

            return true;
        } catch (Exception $e) {
            \Log::info($e->getMessage());

            return false;
        } catch (ClientException $e) {
            \Log::info($e->getMessage());

            return false;
        } catch (ConnectException $e) {
            \Log::info($e->getMessage());

            return false;
        } catch (ServerException $e) {
            \Log::info($e->getMessage());

            return false;
        }
    }
}
