<?php

namespace App\Traits;

trait ModelScopes
{
	
	public function scopeActive($query)
	{
		return $query->where(['status' => 'active']);
	}

	public function scopeInactive($query)
	{
		return $query->where(['status' => 'inactive']);
	}

	public function scopeIncomplete($query)
	{
		return $query->where(['status' => 'incomplete']);
	}

	public function scopePending($query)
	{
		return $query->where(['status' => 'pending']);
	}

	public function scopePaid($query)
	{
		return $query->where(['status' => 'paid']);
	}

	public function scopeProcessed($query)
	{
		return $query->where(['status' => 'processed']);
	}

	public function scopeCancelled($query)
	{
		return $query->where(['status' => 'cancelled']);
	}

	public function scopeSingle($query)
	{
		return $query->where(['type' => 'single']);
	}

	public function scopeMaster($query)
	{
		return $query->where(['type' => 'master']);
	}

	public function scopeChild($query)
	{
		return $query->where(['type' => 'child']);
	}

	public function scopeWeb($query)
	{
		return $query->where(['type' => 'web']);
	}

	public function scopeAccepted($query)
	{
		return $query->whereNotIn('status',['incomplete', 'pending']);
	}
}