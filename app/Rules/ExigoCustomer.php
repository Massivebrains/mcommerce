<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Services\Exigo;
use App\User;
use \Carbon\Carbon;
use Illuminate\Support\Str;

class ExigoCustomer implements Rule
{
    public $error;
    public $message;
    public $customer_is_buying_renewal;

    public function __construct($message = '')
    {
        $this->message                      = strtoupper($message);
        $this->customer_is_buying_renewal   = false;
    }

    public function passes($attribute, $value)
    {
        $user = User::getUserFromMessage($this->message);

        $this->checkIfCustomerIsBuyingRenewal();
        
        $phone = $user ? $user->phone : $value;

        //Force referesh the User Object
        $user = User::getUserFromExigo('phone', $phone);
        
        if(!$user instanceof User){

            $this->error = $user;
            return false;
        }

        if($this->customer_is_buying_renewal){

            if($user){

                if($user->expire_at->diffInDays(now()) > 365){

                    $this->error = 'You cannot renew your subscription anymore. Kindly re-register.';
                    return false;
                }

                //If there is a user who wants to buy renewal, give him 6 hours, just so he/she would be able to complete the order.
                $user->update([

                    'status'    => 'active',
                    'expire_at' => now()->addHours(6)->format('Y-m-d H:i:s')
                ]);

                return true;
            }

            return true;
        }

        if($user instanceof User)
            return true;

        $this->error = $user;

        return false;
    }

    public function checkIfCustomerIsBuyingRenewal()
    {
        if(Str::startsWith($this->message, 'NEO BUY')){

            $products = _find_between($this->message, 'NEO BUY', 'ID');

            if(Str::contains($products, '9999'))
                $this->customer_is_buying_renewal = true;
        }

        if(Str::startsWith($this->message, 'NEO GROUP')){

            $products = _find_between($this->message, 'NEO GROUP', 'ID');

            if(Str::contains($products, '9999'))
                $this->customer_is_buying_renewal = true;
        }
        
    }

    public function message()
    {
        return $this->error;
    }
}
