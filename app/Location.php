<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelScopes;

class Location extends Model
{
	use ModelScopes;
	
    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];

    public function getCurrencyAttribute($value)
    {
    	return strtoupper($value);
    }
    
    public function orders()
    {
    	return $this->hasMany('App\Order');
    }
}
