<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User')->withDefault(function(){

    		return new User();
    	});
    }

    public function order()
    {
    	return $this->belongsTo('App\Order')->withDefault(function(){

    		return new Order();
    	});
    }
}
