<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Validator;
use App\User;
use App\Rules\ExigoCustomer;
use App\Traits\TextResponse;
use App\Http\Controllers\Api\MessageController;

class TelegramExecute implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use TextResponse;

    public $tries = 2;
    public $timeout = 120;

    public $phone;
    public $message;
    public $telegram_id;
    public $name;
    public $username;

    public function __construct($phone = '', $message = '', $telegram_id = 0, $name = '', $username = '')
    {
        $this->phone        = $phone;
        $this->message      = $message;
        $this->telegram_id  = $telegram_id;
        $this->name         = $name;
        $this->username     = $username;
    }


    public function handle()
    {
        $validator = Validator::make(['phone' => $this->phone], [

            'phone' => ['required', new ExigoCustomer($this->message)]
        ]);

        if ($validator->fails()) {
            $this->reply($validator->errors()->first(), 'telegram', null, $this->telegram_id);
        } else {
            User::wherePhone($this->phone)->update([

                'telegram_id'       => $this->telegram_id,
                'telegram_name'     => $this->name,
                'telegram_username' => $this->username
            ]);

            (new MessageController())->execute($this->phone, $this->message, 'telegram');
        }
    }
}
