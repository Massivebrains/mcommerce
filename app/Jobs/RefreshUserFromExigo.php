<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Exigo;
use App\User;

class RefreshUserFromExigo implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        $response = Exigo::get('api/customers/find/'.$this->user->phone);

        if ($response->status == false) {
            $this->user->update([

                'status'    => 'inactive',
                'expire_at' => null
            ]);

            return;
        }

        $user = collect($response->data)->firstWhere('Phone', $this->user->phone);

        if (!$user) {
            $this->user->update([

                'status'    => 'inactive',
                'expire_at' => null
            ]);

            return;
        }

        if ($user && $this->user->status != $user->Status) {
            $this->user->update([

                'status'        => $user->Status,
                'expire_at'     => date('Y-m-d', strtotime($user->ExpiryDate))
            ]);
        }
    }
}
