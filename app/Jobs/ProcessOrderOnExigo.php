<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Order;
use App\Services\Exigo;
use App\Traits\TextResponse;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class ProcessOrderOnExigo implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use TextResponse;

    public $tries = 1;
    public $maxExceptions = 1;
    public $timeout = 240;

    public $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function handle()
    {
        $order = Order::find($this->order->id);

        if ($order->status != 'paid') {
            $order->update(['status' => 'failed_to_process']);

            _order_log($order, 'Order has not been paid for yet. Status :: '.$order->status.' Order Failed to process on Exigo.');

            return;
        }

        if (!$order->transaction_reference) {
            $order->update(['status' => 'failed_to_process']);

            _order_log($order, 'Invalid Transaction Reference :: '.$order->transaction_reference.' Order Failed to process on Exigo.');

            return;
        }

        $products = [];

        foreach ($order->order_details as $row) {
            if (in_array($row->item_code, ['Stamp Duty', '1040', '790'])) {
                continue;
            }

            $products[] = ['itemcode' => $row->item_code, 'quantity' => $row->quantity];
        }

        $notes = "Mcommerce Order Reference: {$order->reference} Location: {$order->location->name}";

        if ($order->type == 'child') {
            $notes .= " Group ID: {$order->master->reference}";
        }

        $payload = [

            'password'              => 'SecretNeolifePassword',
            'reference'             => $order->reference,
            'orderdate'             => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' - 7hours')),
            'neolifeid'             => $order->neolife_id,
            'phone'                 => $order->user->phone,
            'warehouseid'           => $order->location->warehouse_id,
            'shipmethodid'          => $order->location->ship_method,
            'total'                 => $order->total,
            'transactionreference'  => $order->transaction_reference,
            'paymentsource'         => $order->payment_provider->name,
            'products'              => $products,
            'notes'                 => $notes
        ];

        _order_log($order, 'Attemped to process order on Exigo.');

        $response = Exigo::post('order/create', $payload);

        if (optional($response)->status == true) {
            _order_log($order, 'Order Processed successfully on Exigo. Message: '.$response->data);

            $order->update(['status' => 'processed', 'exigo_order_id' => $response->data]);

            if ($order->type == 'single') {
                $message = 'Your Order has been processed. Your Exigo Order ID is '.$response->data;
            } else {
                $message = 'Your Child Order for '.$order->neolife_id.' has been processed. Exigo Order ID is '.$response->data;
            }

            $this->reply($message, $order->source, $order->user);
        } else {
            _order_log($order, 'Order Failed to Process on Exigo. Message: '.$response->data);

            $order->update(['status' => 'failed_to_process', 'description' => $response->data]);

            $error = _get_processing_error_message($order);
            $this->reply($error, $order->source, $order->user);

            $response = (new Client())->post("https://api.neolifeafrica.info/payments/send-email", [

                'headers' => [

                    'Accept' => 'application/json'
                ],

                RequestOptions::JSON => [

                    'to'        => ['oluwaseguno@ng.neolife.com', 'abioduns@ng.neolife.com', 'ogochukwuo@ng.neolife.com', 'collinso@ng.neolife.com', 'adenreleja@ng.neolife.com', 'sylvestered@ng.neolife.com'],
                    'subject'   => 'Mcommerce Order Processing',
                    'body'      => 'Mcomerce Could not process this order. <br><br>OrderID: <b>'.$order->reference.'</b> <br><br>Reason: <i>'.$response->data.'</i> <br> <br> Click <a href="'.url('admin/order/'.$order->id).'">Here</a> to view this order.',
                    'from'      => 'Mcommerce <mcommerce@mdw.neolifeafrica.info>'
                ]
            ]);

            if ((int)$response->getStatusCode() != 200) {
                return (object)['status' => false, 'data' => $response->getBody()->getContents()];
            }

            $response  = json_decode($response->getBody()->getContents());
        }
    }
}
