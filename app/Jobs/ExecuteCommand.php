<?php

namespace App\Jobs;

use App\Traits\Commands\Buy;
use App\Traits\Commands\Cases;
use App\Traits\Commands\Group;
use App\Traits\TextResponse;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ExecuteCommand implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use TextResponse;
    use Buy;
    use Group;
    use Cases;

    public $tries = 2;
    public $timeout = 120;

    public $command;
    public $user;
    public $message;
    public $source;

    public function __construct($command, User $user, $message, $source)
    {
        $this->command = $command;
        $this->user = $user;
        $this->message = $message;
        $this->source = $source;
    }

    public function handle()
    {
        $reply = $this->{$this->command}($this->user, $this->message, $this->source);

        _log($reply, $this->user, 'reply', $this->source);

        $this->reply($reply, $this->source, $this->user);
    }
}
