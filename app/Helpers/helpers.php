<?php

use Illuminate\Support\Str;

function _find_between(string $string, string $start, string $end, bool $greedy = false)
{
    $start = preg_quote($start, '/');
    $end   = preg_quote($end, '/');

    $format = '/(%s)(.*';
    if (!$greedy) {
        $format .= '?';
    }
    $format .= ')(%s)/';

    $pattern = sprintf($format, $start, $end);
    preg_match($pattern, $string, $matches);

    if (count($matches) < 3) {
        return '';
    }

    return trim($matches[2]);
}

function _d($dateString = '', $time = true)
{
    if (!$dateString || $dateString == '') {
        return '00-00-0000';
    }

    if ($time == false) {
        return date('M d, Y', strtotime($dateString));
    }

    return date('M d, Y g:i A', strtotime($dateString));
}

function _api_date($dateString = '', $time = true)
{
    if (!$dateString || $dateString == '') {
        return '00-00-0000';
    }

    if ($time == false) {
        return date('D d, M Y', strtotime($dateString));
    }

    return date('D d, M Y g:i A', strtotime($dateString));
}

function _t($dateString = '')
{
    return date('g:i A', strtotime($dateString));
}

function _c($amount = 0, $currency = '₦')
{
    if (\Auth::check()) {
        switch (\Auth::user()->country_code) {
            case 'BW':
                $currency = 'P';
            break;

            case 'GH':
                $currency = '₵';
            break;

            case 'NG':
                $currency = '₦';
            break;

            case 'TG':
                $currency = 'CFA';
            break;

            default:
            $currency = \Auth::user()->currency_code;

        }
    }

    $amount = (float)$amount;

    if ($amount < 0) {
        return '-'.$currency.number_format($amount*-1, 2);
    }

    return $currency.number_format($amount, 2);
}

function _order_reference()
{
    $reference   =  date('ymd').mt_rand(2000, 9999);

    if (\App\Order::whereReference($reference)->count() > 0) {
        return _order_reference();
    }

    return $reference;
}

function _payment_reference()
{
    return date('ymd').Str::random(8);
}

function _to_k($number = 0)
{
    if ($number > 1000) {
        $x = round($number);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('k', 'm', 'b', 't');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;
    }

    return $number;
}

function _badge($string = '')
{
    $class  = 'secondary';
    $string = strtolower($string);

    if (in_array($string, ['inactive', 'hidden', 'cancelled', 'defaulted', 'failed_to_process', 'no', 'unused'])) {
        $class = 'danger';
    }

    if (in_array($string, ['loan', 'partly_paid', 'pending'])) {
        $class = 'warning';
    }

    if (in_array($string, ['wallet', 'running', 'registration', 'sms'])) {
        $class = 'info';
    }

    if (in_array($string, ['cash', 'bank transfer', 'paystack', 'telegram'])) {
        $class = 'primary';
    }

    if (in_array($string, ['completed', 'active', 'processed', 'accepted', 'yes', 'paid', 'used', 'whatsapp'])) {
        $class = 'success';
    }

    if (in_array($string, ['whatsapp'])) {
        $class = 'whatsapp';
    }

    $string = strtoupper($string);

    echo "<span class='badge badge-{$class} text-uppercase'>{$string}</span>";
}


function _email($to = '', $subject = 'iddera.ng', $body = '')
{
    try {
        if (env('APP_ENV', 'local') != 'production') {
            return;
        }

        if ($to == '' || $body == '') {
            return false;
        }

        $sendgrid = new \SendGrid('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');

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom('no-reply@Iddera.ng', 'Iddera.ng');
        $email->setSubject($subject);
        $email->addTo($to);

        if ($to != 'vadeshayo@gmail.com') {
            $email->addBCC('vadeshayo@gmail.com');
        }


        $email->addContent('text/html', $body);

        $response = $sendgrid->send($email);

        //dd($response->body());

        return true;
    } catch (Exception $e) {
        return false;
    }
}

function _is_nuban_valid($input = '')
{
    $nuban = str_split($input);

    $total = ($nuban[0] * 3) + ($nuban[1] * 7) + ($nuban[2] * 3) + ($nuban[3] * 3) + ($nuban[4] * 7) + ($nuban[5] * 3) + ($nuban[6] * 3) + ($nuban[7] * 7) + ($nuban[8] * 3) + ($nuban[9] * 3) + ($nuban[10] * 7) + ($nuban[11] * 3);

    $check_digit = (10 - ($total % 10)) == 10 ? 0 : (10 - ($total % 10));

    if ($check_digit != $nuban[12]) {
        return false;
    }

    return true;
}

function _to_phone($string = '', $remove234 = false)
{
    if ($remove234) {
        return '0'.substr($string, -10);
    }

    return '234'.substr($string, -10);
}

function _recharge_code_sms($recharge_code = null)
{
    $start_at   = _api_date($recharge_code->start_at);
    $end_at     = _api_date($recharge_code->end_at);

    $sms = "A new Recharge Code has been Generated for You %0a";
    $sms .= "Code:{$recharge_code->code} %0a";
    $sms .= "Device:$recharge_code->product_serial %0a";
    $sms .= "Start Date:$start_at %0a";
    $sms .= "End Date:$end_at %0a";

    return $sms;
}

function _order_log($order = null, $log)
{
    if ($order instanceof \App\Order) {
        \App\Log::create([

            'user_id'   => $order->user->id,
            'order_id'  => $order->id,
            'log'       => $log
        ]);
    }

    if ($order instanceof \App\GroupOrder) {
        \App\Log::create([

            'user_id'         => $order->user->id,
            'group_order_id'  => $order->id,
            'log'             => $log
        ]);
    }

    return true;
}

function _log($log = '', \App\User $performedOn = null, $type = '', $source = '')
{
    $properties = ['type' => $type, 'source' => $source];

    if (\Auth::check()) {
        $user = \Auth::user();

        if ($performedOn != null) {
            return activity()
            ->performedOn($performedOn)
            ->causedBy($user)
            ->withProperties($properties)
            ->log((string)$log);
        }

        return activity()->causedBy($user)->withProperties($properties)->log((string)$log);
    }

    if ($performedOn != null) {
        return activity()->performedOn($performedOn)->withProperties($properties)->log((string)$log);
    }

    return activity()->withProperties($properties)->log((string)$log);
}

function _get_info()
{
    $info = 'Dear distributor, the format used to send Mcommerce order message will change from the month of January, after December month end.'.PHP_EOL.PHP_EOL;
    $info .= 'This is the new format -  NEO BUY 2130 2, 2315 3 ID 063-1234567 DC 10 BANK 6'.PHP_EOL.PHP_EOL;
    $info .= 'or NEO GROUP 2130 2, 2315 3 ID 063-1234567 DC 10 BANK 6'.PHP_EOL.PHP_EOL;
    $info .= 'The difference between the new and the old format is the addition of the country code to your ID. Instead of 1223456 you will now use 063-1223456.'.PHP_EOL.PHP_EOL;
    $info .= 'Kindly pass this message to your team members.'.PHP_EOL.PHP_EOL.PHP_EOL;
    $info .= 'Thank you for choosing NeoLife.';

    return $info;
}

function _get_processing_error_message($order)
{
    $numbers = [
        8 => 2348131725943,
        9 => 2348131725942,
        10 => 2348131771534,
        11 => 2348033405813,
        12 => 2348123944868,
        13 => 2349087207192,
        14 => 2348178688916,
        15 => 2348130698225,
        16 => 2349037561635,
        17 => 2348138240732,
        70 => 2347061634327,
    ];

    $warehouse_id = $order->location->warehouse_id;

    if (isset($numbers[$warehouse_id])) {
        $phone = $numbers[$warehouse_id];
    } else {
        $phone = '08131771534';
    }

    return 'We received your payment for order with ID '.$order->reference.' but your order is incomplete. Kindly contact '.$phone.' for more information.';
}
