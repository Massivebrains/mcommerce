<?php 

namespace App\Services;

use \GuzzleHttp\Client;
use \GuzzleHttp\RequestOptions;
use \GuzzleHttp\Exception\ClientException;
use \GuzzleHttp\Exception\ConnectException;
use \GuzzleHttp\Exception\ServerException;

class Exigo {

	public static function get($endpoint = '')
	{
		try{

			$baseUrl = config('app.EXIGO_ENDPOINT');

            $response = (new Client)->get("{$baseUrl}/{$endpoint}", [

                'headers' => [

                    'Accept' => 'application/json'
                ]
            ]);

            if((int)$response->getStatusCode() != 200)
                return (object)['status' => false, 'data' => $response->getBody()->getContents()];

            $response = json_decode($response->getBody()->getContents());
            $data     = $response;

            if(isset($response->response))
                $data = $response->response;

            return (object)['status' => $response->status ?? true, 'data' => $data];

        }catch(Exception $e){

            return (object)['status' => false, 'data' => $e->getMessage()];

        }catch(ClientException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];

        }catch(ConnectException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];
            
        }catch(ServerException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];
        }
	}

    public static function post($endpoint = '', $payload = [])
    {
        try{

            $baseUrl = config('app.EXIGO_ENDPOINT');

            $response = (new Client)->post("{$baseUrl}/$endpoint", [

                'headers' => [

                    'Accept' => 'application/json'
                ],

                RequestOptions::JSON => $payload
            ]);

            if((int)$response->getStatusCode() != 200)
                return (object)['status' => false, 'data' => $response->getBody()->getContents()];

            $response   = json_decode($response->getBody()->getContents());
            $data       = $response;

            if(isset($response->response))
                $data = $response->response;

            return (object)['status' => $response->status ?? true, 'data' => $data];

        }catch(Exception $e){

            return (object)['status' => false, 'data' => $e->getMessage()];

        }catch(ClientException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];

        }catch(ConnectException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];
            
        }catch(ServerException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];
        }
    }
}