<?php 

namespace App\Services;

class Command
{
    const BUY = 'buy';
    const GROUP = 'group';
    const JOIN = 'join_telegram';
    const NONE = 'none';
}