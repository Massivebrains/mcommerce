<?php 

namespace App\Services;

use App\User;
use Illuminate\Support\Str;
use App\Services\Command;

class Intent {

	public static function getCommand(User $user, $message)
	{
		if(Str::startsWith($message, 'NEO BUY') || in_array($message, ['1']))
			return Command::BUY;

		if(Str::startsWith($message, 'NEO GROUP') || in_array($message, ['2', 'CANCEL']))
			return Command::GROUP;

		if(Str::startsWith($message, 'NEO JOIN') || in_array($message, ['/START']));
			return Command::JOIN;

		return Command::BUY;
	}
}