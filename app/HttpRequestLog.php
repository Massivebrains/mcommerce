<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class HttpRequestLog extends Model
{
    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];

    public function setHeadersAttribute($value)
    {
        $this->attributes['headers'] = json_encode($value);
    }

    public function setRequestAttribute($value)
    {
        $this->attributes['request'] = json_encode($value);
    }

    public function setResponseAttribute($value)
    {
        $this->attributes['response'] = json_encode($value);
    }

    public static function logHttpRequest(Request $request, $response = []) : self
    {
        return self::create([
            'ip' => $request->ip(),
            'headers' => $request->header(),
            'request' => $request->all(),
            'response' => $response
        ]);
    }
}
