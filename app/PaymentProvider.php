<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelScopes;

class PaymentProvider extends Model
{
    use ModelScopes;

    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function formatMessage($object)
    {
        $message    = $this->message;
        $app_url    =  env('APP_URL', 'https://mcommerce.neolifeafrica.info');
        $url        = "{$app_url}/p/{$object->payment_reference}";

        $message = str_replace('{url}', $url, $message);
        $message = str_replace('{reference}', $object->reference, $message);
        $message = str_replace('{amount}', $object->total, $message);

        if (in_array($this->id, [1, 2, 3, 4])) {
            $message .= PHP_EOL.PHP_EOL."You can also use this link to pay with your debit card: ".$url;
        }

        return $message;
    }

    public function logHttpRequest($request = [], $headers = [], $response = []) : void
    {
        HttpRequestLog::create([
            'scope' => 'payment_provider',
            'scope_id' => $this->id,
            'headers' => $headers,
            'request' => $request,
            'response' => $response
        ]);
    }
}
