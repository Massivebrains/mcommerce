<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ExigoCustomer;
use Illuminate\Support\Str;
use App\Traits\Responders\WhatsappResponder;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\User;
use Validator as RequestValidator;

class WhatsappMessageRequest extends FormRequest
{
    use WhatsappResponder;

    protected function failedValidation(Validator $validator)
    {
        $error = $validator->errors()->first();

        throw new HttpResponseException(response($error, 200)->header('Content-Type', 'text/plain'));
    }

    public function rules()
    {
        return [

            'contacts'     => 'required|array',
            'messages'     => 'required|array'
        ];
    }

    public function getPhone()
    {
        $phone = optional(request('messages')[0])['from'];

        if (!$phone) {
            throw new HttpResponseException(response('Whatsapp Phone number cannot be detected', 200)->header('Content-Type', 'text/plain'));
        }

        $response = User::getUserFromExigo('phone', $phone);

        if ($response instanceof User) {
            return $response->phone;
        }

        throw new HttpResponseException(response($response, 200)->header('Content-Type', 'text/plain'));
    }

    public function getMessage()
    {
        $message = optional(request('messages')[0])['text'];

        if (!$message) {
            throw new HttpResponseException(response('Whatsapp message cannot be detected', 200)->header('Content-Type', 'text/plain'));
        }

        if (!isset($message['body'])) {
            throw new HttpResponseException(response('Whatsapp message cannot be detected', 200)->header('Content-Type', 'text/plain'));
        }

        $message = $message['body'];

        if (Str::contains($message, 'NEO')) {
            $user = User::getUserFromMessage($message);

            $validator = RequestValidator::make(['phone' => optional($user)->phone], [

                'phone' => ['required', new ExigoCustomer($message)]
            ]);

            if ($validator->fails()) {
                throw new HttpResponseException(response($validator->errors()->first(), 200)->header('Content-Type', 'text/plain'));
            }
        }

        return $message;
    }
}
