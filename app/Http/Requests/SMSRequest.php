<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ExigoCustomer;
use Illuminate\Support\Str;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SMSRequest extends FormRequest
{
    public function rules()
    {
        return [

            'phone'     => ['required', 'min:11', new ExigoCustomer(request('message'))],
            'message'   => 'required'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $error = $validator->errors()->first();

        throw new HttpResponseException(response($error, 200)->header('Content-Type', 'text/plain'));
    }

    public function validationData()
    {
        return $this->route()->parameters();
    }
}
