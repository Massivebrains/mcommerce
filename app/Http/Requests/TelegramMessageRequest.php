<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class TelegramMessageRequest extends FormRequest
{
    public $id         = null;
    public $name       = null;
    public $username   = null;
    public $user       = null;
    public $message    = null;

    public function __construct()
    {
        $message = request('message');

        if(!$message)
            return;

        $chat = $message['chat'] ?? null;

        if(!$chat)
            return;

        $username   = isset($chat['username']) ? $chat['username'] : '';
        $first_name = isset($chat['first_name']) ? $chat['first_name'] : '';
        $last_name  = isset($chat['last_name']) ? $chat['last_name'] : '';

        $this->id               = $chat['id'] ?? null;
        $this->username         = $username;
        $this->name             = $first_name.' '.$last_name;
        $this->message          = strtoupper($message['text'] ?? null);

        $user = User::whereTelegramId($this->id)->first();

        if($user)
          $this->user = $user;
   }

   public function rules()
   {
        return [];
   }
}
