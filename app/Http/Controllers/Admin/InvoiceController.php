<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use Auth;

class InvoiceController extends Controller
{
    public function index()
    {
        $data['title'] 		= 'Invoice';
        $data['active']		= 'invoices';

        return view('admin.invoices.index');
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'start_at'		=> 'required|date',
            'end_at'		=> 'required|date',
            'description'	=> 'required',
            'country_code'  => 'required'
        ]);

        $start_at 	= request('start_at');
        $end_at		= request('end_at');
        $country    = request('country_code');

        $web = Order::query()
        ->whereStatus('paid')
        ->whereBetween('paid_at', [$start_at, $end_at])
        ->whereCountryCode($country)
        ->where('type', 'web')
        ->get();

        $orders_count = Order::query()
        ->whereIn('status', ['paid', 'processed', 'failed_to_process'])
        ->whereNotIn('type', ['master', 'web'])
        ->whereBetween('paid_at', [$start_at, $end_at])
        ->whereCountryCode($country)
        ->count();

        $web_sum = 0;
        foreach ($web as $row) {
            $web_sum += .015 * $row->total > 2000 ? 2000 : .015 * $row->total;
        }

        $data['start_at']		= $start_at;
        $data['end_at']			= $end_at;
        $data['web']			= $web;
        $data['web_sum']		= $web_sum;
        $data['orders_count']	= $orders_count;
        $data['description']	= request('description');
        $data['country']        = $country;

        return view('admin.invoices.invoice', $data);
    }
}
