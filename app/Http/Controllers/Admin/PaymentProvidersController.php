<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PaymentProvider;
use Auth;

class PaymentProvidersController extends Controller
{
    public function index()
    {
    	
    	$data['title'] 				= 'Payment Providers';
    	$data['active']				= 'banks';
    	$data['payment_providers']	= PaymentProvider::whereCountryCode(Auth::user()->country_code)->paginate(50);
    	
    	return view('admin.payment-providers.index', $data);
    }
}
