<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use DB;
use App\User;
use Auth;
use App\Traits\TextResponse;

class ConversationsController extends Controller
{
    use TextResponse;

    public function index($source = 'sms')
    {
    	if(request('source'))
    		$source = request('source');

    	$data['active']	= $source;
    	$data['source']	= $source;

    	$activities = Activity::select(DB::raw('max(users.id) as user_id, max(users.neolife_id) as neolife_id, max(users.phone) as phone, max(users.name) as name, max(subject_id) as subject_id, count(activity_log.id) as messages_count, max(activity_log.created_at) as last_message_date'))
    	->groupBy('subject_id')
    	->join('users', 'users.id', 'activity_log.subject_id')
    	->where('users.country_code', Auth::user()->country_code)
    	->where('properties', 'like', "%$source%");

    	if(request('query') != null)
    		$activities->where('users.neolife_id', request('query'));

    	$activities = $activities->orderBy(DB::raw('max(activity_log.created_at)'), 'desc')
    	->take(10)
    	->get();

    	$data['activities'] = $activities;

    	return view('admin.conversations.index', $data);
    }

    public function conversation($source = 'sms', $user_id = 0)
    {
    	$user = User::find($user_id);

    	if(!$user)
    		return abort(404);

    	$data['active']	= $source;
    	$data['source']	= $source;
    	$data['user']	= $user;

    	$activities = Activity::whereSubjectId($user_id)
    	->where('properties', 'like', "%$source%")
    	->orderBy('id', 'desc')
    	->take(100)
    	->get();

    	$data['activities'] = $activities;

    	return view('admin.conversations.conversation', $data);
    }

    public function sendMessage(Request $request)
    {
        $this->validate($request, [

            'message'   => 'required'
        ]);

        $user       = User::find(request('user_id'));
        $message    = request('message');
        $source     = request('source');

        if(!$user)
            return redirect()->back()->with('error', 'Invalid User selected.');

        if($source == 'telegram'){

            if($user->telegram_id < 1)
                return redirect()->back()->with('error', 'This User has not used telegram before.');

            $this->reply($message, 'telegram', $user);
        }

        if($source == 'whatsapp'){

            $this->reply($message, 'whatsapp', $user);
        }

        if($source == 'sms')
            return redirect()->back()->with('error', 'You cannot send SMS to customers from this portal. only Telegram and Whatsapp is allowed.');

        _log($message.' [SUPPORT by '.Auth::user()->name.']', $user, 'reply', $source);

        return redirect()->back()->with('message', 'Message Sent successfully');
    }
}
