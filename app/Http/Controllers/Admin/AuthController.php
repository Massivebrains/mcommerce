<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Exception;
use \GuzzleHttp\Exception\ClientException;
use \GuzzleHttp\Exception\ConnectException;

class AuthController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            if (Auth::user()->type == 'admin') {
                return redirect('admin/orders/single');
            }

            return redirect('/logout');
        }

        return view('admin.auth.index');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'country'  => 'required',
            'password' => 'required'
        ]);

        $validCredentials = $this->loginAsKiosk() || $this->loginViaAPI();

        if ($validCredentials) {
            $user = User::updateOrCreate([
                'customer_id'   => 0,
                'neolife_id'    => 'Staff',
                'phone'         => 0,
                'name'          => request('username'),
                'country_code'  => request('country'),
                'status'        => 'active',
                'type'          => 'admin',
                'access_level'  => 1
            ]);

            Auth::loginUsingId($user->id);
            if ($this->loginAsKiosk()) {
                Auth::user()->update(['access_level' => 0]);
            }
            return redirect('/admin/orders/single');
        }

        return redirect()->back()->with('error', 'Invalid Username or Password');
    }

    public function loginViaAPI()
    {
        try {
            $client = new Client(['base_uri' => 'https://mdw.neolifeafrica.info']);

            $options = [

                'json' => [

                    'LoginName' => request('username'),
                    'Password'  => request('password')
                ]
            ];
            $response = $client->post('mcommerce/api/auth', $options);
            $data = json_decode($response->getBody());
            return (bool)$data->status;
        } catch (Exception $e) {
            return false;
        } catch (ClientException $e) {
            return false;
        } catch (ConnectException $e) {
            return false;
        }
    }

    public function loginAsKiosk()
    {
        foreach (User::kioskUsers() as $row) {
            if ($row['username'] == request('username') && $row['password'] == request('password')) {
                return true;
            }
        }
        return false;
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/login')->with('message', 'You have been successfully logged out.');
    }
}
