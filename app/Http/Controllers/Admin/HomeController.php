<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function index()
    {
    	$country 		= Auth::user()->country_code;
    	$start_date		= Carbon::startOfMonth()->format('Y-m-d');
    	$end_date		= Carbon::endOfMonth()->format('Y-m-d');

    	$data['endpoint'] = "https://metabase.neolifeafrica.info/public/dashboard/e5f12209-2e2c-4539-b799-46b98042bab2?country=$country&start_date=$start_date&end_date=$end_date";

    	return view('admin.dashboard.index', $data);
    }
}
