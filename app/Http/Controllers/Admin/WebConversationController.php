<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\MessageController;
use App\User;

class WebConversationController extends Controller
{
    public function index()
    {
    	$data['active']	= 'web';

    	return view('admin.conversations.web', $data);
    }

    public function getUser($neolife_id = null)
    {
    	$user = User::getUserFromExigo('neolife_id', $neolife_id);

    	if($user instanceof User){

    		return response()->json(['status' => true, 'data' => $user]);
    	}

    	return response()->json(['status' => false, 'data' => $user]);
    }

    public function message(Request $request)
    {
    	$this->validate($request, [

    		'phone'		=> 'required|exists:users,phone',
    		'message'	=> 'required',
    	]);

    	$response = (new MessageController)->execute(request('phone'), request('message'), 'sms');

    	return response()->json($response);
    }
}
