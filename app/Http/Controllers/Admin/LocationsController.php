<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Location;
use Auth;

class LocationsController extends Controller
{
    public function index()
    {
    	
    	$data['title'] 		= 'Payment Providers';
    	$data['active']		= 'locations';
    	$data['locations']	= Location::whereCountryCode(Auth::user()->country_code)->paginate(50);
    	
    	return view('admin.locations.index', $data);
    }
}
