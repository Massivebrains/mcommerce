<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\Jobs\ProcessOrderOnExigo;
use App\PaymentProvider;
use App\Traits\Pay;
use Auth;

class OrderController extends Controller
{
    use Pay;

    public function index($id = 0)
    {
        $order = Order::find($id);

        if (!$order) {
            return redirect()->back()->with('error', 'Order Not Found.');
        }

        $data['title']  = 'Order '.$order->reference;
        $data['order']  = $order;
        $data['color']  = $this->getColor($order->status);

        return view('admin.orders.order.index', $data);
    }

    private function getColor($status = '')
    {
        if (in_array($status, ['incomplete', 'pending', 'failed_to_process'])) {
            return 'danger';
        }

        if (in_array($status, ['paid', 'processed'])) {
            return 'success';
        }

        return 'primary';
    }

    public function resolvePaymentForm($id = 0)
    {
        $order = Order::where(['id' => request('id'), 'status' => 'pending'])->first();

        if (!$order) {
            return redirect()->back()->with('error', 'Invalid Order.');
        }

        $data['order']                  = $order;
        $data['payment_providers']     = PaymentProvider::whereCountryCode(Auth::user()->country_code)->get();

        return view('admin.orders.order.resolve-payment', $data);
    }

    public function resolvePayment(Request $request)
    {
        $this->validate($request, [

            'id'                        => 'required|exists:orders,id',
            'payment_provider_id'       => 'required|exists:payment_providers,id',
            'transaction_reference'     => 'required',
            'description'               => 'required'
        ]);

        $order = Order::where(['id' => request('id'), 'status' => 'pending'])->first();

        if (!$order) {
            return redirect()->back()->with('error', 'Invalid Order.');
        }

        $name               = Auth::user()->name;
        $payment_provider   = PaymentProvider::find(request('payment_provider_id'));
        $description        = request('description');

        _order_log($order, "$name attempts to update a pending order to paid.");

        if ($order->payment_provider_id != $payment_provider->id) {
            _order_log($order, "$name attempts to update order from a different payment provider. Payment Provider sent is $payment_provider->name ");

            $order->update(['payment_provider_id' => $payment_provider->id]);
        }

        $this->payForOrder($order, request('transaction_reference'));

        _order_log($order, "Payment Description from Bank Statement is : $description");
        _order_log($order, "Order status changed from pending to paid by $name");

        return redirect('/admin/order/'.$order->id)->with('message', 'Order update successfully.');
    }

    public function retry($id = 0)
    {
        $order = Order::where(['id' => $id])->whereIn('status', ['paid', 'failed_to_process'])->first();

        if (!$order) {
            return redirect()->back()->with('error', 'Invalid Order.');
        }

        _order_log($order, 'Attempts to retry order as it previously failed to process by '.Auth::user()->name);

        $order->update(['status' => 'paid', 'description' => null]);

        _order_log($order, 'Order status changed from failed_to_process to paid');

        ProcessOrderOnExigo::dispatch($order);

        return redirect()->back()->with('message', 'Order pushed back into the processing queue.');
    }

    public function ignore($id = 0)
    {
        $order = Order::where(['id' => $id, 'status' => 'failed_to_process'])->first();

        if (!$order) {
            return redirect()->back()->with('error', 'Invalid Order. You can only ignore an order that has previously failed to process.');
        }

        _order_log($order, 'Attempts to ignore order as it previously failed to process');

        $name = Auth::user()->name;
        $order->update(['status' => 'processed', 'description' => $order->description.'(Ignored by '.$name.')']);

        _order_log($order, 'Order status changed from failed_to_process to processed without creating the order on exigo.');

        return redirect()->back()->with('message', 'Order has been successfully ignored.');
    }
}
