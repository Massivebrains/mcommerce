<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Http\Controllers\Controller;
use App\Order;
use Auth;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function single()
    {
        $country_code = Auth::user()->country_code;

        $data['active'] = 'single-orders';
        $data['orders'] = Order::whereCountryCode($country_code)
            ->single()
            ->whereNotIn('status', ['incomplete', 'cancelled'])
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('admin.orders.singles', $data);
    }

    public function group()
    {
        $country_code = Auth::user()->country_code;

        $data['active'] = 'group-orders';

        $data['orders'] = Order::whereCountryCode($country_code)
            ->master()
            ->whereNotIn('status', ['incomplete', 'cancelled'])
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('admin.orders.masters', $data);
    }

    public function web()
    {
        $country_code = Auth::user()->country_code;

        $data['active'] = 'web-orders';

        $data['orders'] = Order::whereCountryCode($country_code)
            ->web()
            ->whereNotIn('status', ['cancelled'])
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('admin.orders.web', $data);
    }

    public function search()
    {
        $orders = Order::search(request('query'));

        $data['print'] = false;

        if ($orders->count() == 1 && request('submit') == 'print') {
            $data['print'] = true;
        }

        $data['orders'] = $orders;
        return view('admin.orders.search', $data);
    }
}
