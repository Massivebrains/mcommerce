<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\Traits\Pay;

class OrderController extends Controller
{
  use Pay;

  public function pay(Request $request)
  {
    $this->validate($request, [

      'access_token'           => 'required',
      'payment_reference'      => 'required', 
      'transaction_reference'  => 'required'

    ]);

    if(request('access_token') != 'password'){

      return response()->json(['status' => false, 'message' => 'Invalid Access Token', 'data'   => null ], 422);
    }

    $order = Order::where(['reference' => request('payment_reference'), 'status' => 'pending'])->first();

    if(!$order){

      return response()->json([

        'status'    => false, 
        'message'   => 'Invalid Order Reference Provided', 
        'data'      => null

      ], 422);
    }

    if($this->payForOrder($order, request('transaction_reference'))){

      return response()->json([

        'status'    => true, 
        'message'   => 'Order has ben updated accordingly', 
        'data'      => null

      ]);
    }

    return response()->json([

      'status'    => false, 
      'message'   => 'Order could not be updated. Payment failed', 
      'data'      => null 
    ]);

  }

}
