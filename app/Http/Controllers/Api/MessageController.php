<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\SMSRequest;
use App\Http\Requests\WhatsappMessageRequest;
use App\Http\Requests\TelegramMessageRequest;
use App\User;
use Illuminate\Support\Str;
use App\Services\Intent;
use App\Traits\TextResponse;
use App\Traits\Commands\Buy;
use App\Traits\Commands\Group;
use App\Traits\Commands\Cases;
use Spatie\Activitylog\Models\Activity;
use Carbon\Carbon;
use App\Jobs\ExecuteCommand;
use App\Jobs\RefreshUserFromExigo;
use Validator;
use App\Services\Exigo;
use App\Rules\ExigoCustomer;
use GuzzleHttp\Client;
use App\Jobs\TelegramExecute;
use Exception;

class MessageController extends Controller
{
    use TextResponse;
    use Buy;
    use Group;
    use Cases;

    public $user;

    public function execute($phone, $message, $source = 'sms', $meta = [])
    {
        $message    = urldecode($message);
        $user       = User::wherePhone($phone)->first();

        if (!$user) {
            return $this->reply();
        }

        try {
            $last_activity  = Activity::where(['subject_id' => $user->id])->orderBy('id', 'desc')->first();

            if ($last_activity && $last_activity->created_at->diffInSeconds(Carbon::now()) < 20) {
                if ($source == 'sms') {
                    return $this->reply();
                }

                if ($last_activity->description == $message) {
                    return $this->reply();
                }
            }

            $message    = str_replace('  ', ' ', trim(strtoupper($message)));
            $command    = Intent::getCommand($user, $message);

            _log($message, $user, 'message', $source);

            if ($command && method_exists($this, $command)) {
                if ($source == 'sms') {
                    $reply = $this->$command($user, $message, $source, $meta);
                    _log($reply, $user, 'reply', $source);

                    return $this->reply($reply);
                }

                ExecuteCommand::dispatch($command, $user, $message, $source, $meta)->onQueue('priority');
                RefreshUserFromExigo::dispatch($user);

                return $this->reply();
            }

            return $this->reply(__('errors.invalid_command'), $source, $user);
        } catch (Exception $e) {
            return $this->reply(__('errors.exception', ['error' => $e->getMessage()]), $source, $user);
        }
    }

    public function sms(SMSRequest $request)
    {
        return $this->execute($request->phone, $request->message, 'sms');
    }

    public function whatsapp(WhatsappMessageRequest $request)
    {
        return $this->execute($request->getPhone(), $request->getMessage(), 'whatsapp');
    }

    public function telegram(TelegramMessageRequest $request)
    {
        if ($request->user == null) {
            return $this->joinTelegram($request);
        }

        if ($request->user->status == 'active' && !Str::contains($request->message, 'NEO')) {
            return $this->execute($request->user->phone, $request->message, 'telegram');
        }

        TelegramExecute::dispatch($request->user->phone, $request->message, $request->user->telegram_id)->onQueue('priority');

        return $this->reply();
    }

    private function joinTelegram(TelegramMessageRequest $request)
    {
        if ($request->message == '/START') {
            return $this->reply(__('messages.telegram_start'), 'telegram', null, $request->id);
        }

        if (!Str::startsWith($request->message, 'NEO JOIN')) {
            return $this->reply(__('errors.invalid_command'), 'telegram', null, $request->id);
        }

        $phone = trim(Str::after($request->message, 'JOIN'));

        if (!$phone) {
            return $this->reply(__('errors.invalid_neo_join'), 'telegram', null, $request->id);
        }

        $user = User::wherePhone($phone)->first();

        if ($user) {
            if ($user->telegram_id && $user->telegram_id != $request->id) {
                return $this->reply(__('errors.phone_number_link_invalid'), null, $request->id);
            }

            $user->update([

                'telegram_id'       => $request->id,
                'telegram_name'     => $request->name,
                'telegram_username' => $request->username
            ]);

            return $this->reply(__('errors.already_joined_telegram'), 'telegram', null, $request->id);
        }

        TelegramExecute::dispatch($phone, __('messages.joined_successfully'), $request->id, $request->name, $request->username);

        return $this->reply(__('messages.joined_successfully'), 'telegram', null, $request->id);
    }

    public function telegramEmpty()
    {
        return response()->json(true);
    }

    public function fallback()
    {
        return $this->reply('Opzz. Could not process your request. Please send your message again.');
    }

    public function getWebhook()
    {
        return redirect('https://api.telegram.org/bot295972786:AAHQahIa3fFI0q_IK1i8RFP-3N8pQpYpV_4/getWebhookInfo');
    }

    public function setWebhook()
    {
        //$this->telegramSetWebHook('https://23b871c4ad96.ngrok.io/api/v1/telegram-empty');
        $this->telegramSetWebHook('https://mcommerce.neolifeafrica.info/api/v1/telegram');
    }

    public function flushRedis()
    {
        \Redis::command('flushdb');
        return response()->json(true);
    }
}
