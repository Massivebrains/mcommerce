<?php

namespace App\Http\Controllers\Api\Payments\Gateways;

use App\Http\Controllers\Controller;
use App\HttpRequestLog;
use Illuminate\Http\Request;
use App\Order;
use App\Traits\Pay;
use App\PaymentProvider;
use Validator;

class USSDController extends Controller
{
    use Pay;

    public function __invoke(Request $request)
    {
        $httpRequestLog = HttpRequestLog::logHttpRequest($request);
        $validator = $this->getValidator($request);

        if ($validator->fails()) {
            return $this->error($validator->errors()->first(), $httpRequestLog);
        }

        $data               = $this->getData();
        $payment_provider   = PaymentProvider::whereApiToken($data->api_token)->first();
        $order              = Order::where(['reference' => $data->reference, 'status' => 'pending'])->first();

        if (!$order) {
            _order_log($order, "Order Status is not pending as at the time {$payment_provider->name} attempts to update this order status.");

            return $this->error('Order already paid for.', $httpRequestLog);
        }

        $httpRequestLog->update(['order_id' => $order->id]);
        _order_log($order, "{$payment_provider->name} Attempted to update order status to paid");
        _order_log($order, "{$payment_provider->name} Sent Info ::=> AmountPaid: {$data->amount}, Description: {$data->description}, Status: {$data->status} Reference:{$data->transaction_reference}");

        if ((float)request('amount') < ($order->total - $payment_provider->charge)) {
            _order_log($order, "Amount sent by {$payment_provider->name} is smaller than order total. No changes will be made.");

            return $this->error('Amount is invalid', $httpRequestLog);
        }

        if ($order->payment_provider_id != $payment_provider->id) {
            _order_log($order, "A different payment provider will be used to pay for this order. Payment provider will be updated from {$order->payment_provider->name} to {$payment_provider->name}");

            $order->update(['payment_provider_id' => $payment_provider->id ]);

            if ($order->type == 'master') {
                foreach ($order->children as $row) {
                    _order_log($row, "Master order payment type has changed.");

                    $row->update(['payment_provider_id' => $payment_provider->id ]);
                }
            }
        }

        if ($data->status != '00') {
            _order_log($order, "Payment Failed. Status: {$data->status}");

            return $this->error('Payment failed.', $httpRequestLog);
        }

        if ($this->payForOrder($order, $data->transaction_reference)) {
            return $this->updateSuccess('Order was updated successfully.', ['amount' => $order->total], $httpRequestLog);
        }

        return $this->error('Order could not be updated. No changes was made.', $httpRequestLog);
    }

    private function getValidator(Request $request)
    {
        if (request('key') && request('order_id')) {
            return Validator::make($request->all(), [

                'key'                   => 'required|exists:payment_providers,api_token',
                'order_id'              => 'required|exists:orders,reference',
                'amount'                => 'required|gt:0',
                'status_ref'            => 'required',
                'trans_ref'             => 'required',
                'desc'                  => '',
                'hash'                  => ''
            ]);
        } else {
            return Validator::make($request->all(), [

                'api_token'             => 'required|exists:payment_providers,api_token',
                'reference'             => 'required|exists:orders,reference',
                'amount'                => 'required|gt:0',
                'status'                => 'required',
                'transaction_reference' => 'required',
                'description'           => '',
                'hash'                  => ''
            ]);
        }
    }

    private function getData()
    {
        $api_token              = null;
        $reference              = null;
        $amount                 = null;
        $status                 = null;
        $transaction_reference  = null;
        $description            = null;

        if (request('key') && request('order_id')) {
            $api_token              = request('key');
            $reference              = request('order_id');
            $amount                 = request('amount');
            $status                 = request('status_ref');
            $transaction_reference  = request('trans_ref');
            $description            = request('desc');
        } else {
            $api_token              = request('api_token');
            $reference              = request('reference');
            $amount                 = request('amount');
            $status                 = request('status');
            $transaction_reference  = request('transaction_reference');
            $description            = request('description');
        }

        return (object)[

            'api_token'                 => $api_token,
            'reference'                 => $reference,
            'amount'                    => $amount,
            'status'                    => $status,
            'transaction_reference'     => $transaction_reference,
            'description'               => $description
        ];
    }

    private function error($message = '', $httpRequestLog = null)
    {
        if ($httpRequestLog) {
            $httpRequestLog->update(['response' => ['message' => $message]]);
        }

        return response()->json([

            'status'    => 'error',
            'message'   => $message,
            'data'      => null
        ]);
    }

    private function updateSuccess($message = '', $data = [], $httpRequestLog = null)
    {
        if ($httpRequestLog) {
            $httpRequestLog->update(['response' => ['message' => $message]]);
        }

        return response()->json([

            'status'    => 'success',
            'message'   => $message,
            'data'      => $data
        ]);
    }
}
