<?php

namespace App\Http\Controllers\Api\Payments\Gateways;

use App\Http\Controllers\Controller;
use App\HttpRequestLog;
use Illuminate\Http\Request;
use App\Order;
use App\PaymentProvider;
use App\Traits\Pay;

class MDWBankController extends Controller
{
    use Pay;

    public function pay($reference = '')
    {
        if (request()->query('Status')) {
            return view('payments.mdw.error', ['error' => request()->query('Error')]);
        }

        $order = Order::wherePaymentReference($reference)->first();

        if (!$order) {
            return view('payments.mdw.error', ['error' => __('errors.mdw_invalid_order')]);
        }

        if ($order->status == 'paid') {
            return view('payments.mdw.error', ['error' => __('errors.mdw_already_paid')]);
        }

        if ($order->status != 'pending') {
            return view('payments.mdw.error', ['error' => __('errors.mdw_invalid_order')]);
        }

        if (in_array($order->payment_provider_id, [1, 2, 3, 4])) {
            _order_log($order, "Customer clicked on Payment Link. Updating payment provider to Paystack");
            $provider = PaymentProvider::whereProvider('Paystack')->first();

            if ($provider) {
                $order->update(['payment_provider_id' => $provider->id]);

                if ($order->type == 'master') {
                    Order::whereGroupOrderId($order->id)->update(['payment_provider_id' => $provider->id]);
                }
            }
        }

        $data['order']          = $order;
        $data['currency']       = $order->location->currency;
        $data['description']    = 'Payment for Mcommerce Order '.$order->reference.' with Total '.$order->amount;
        $data['url']            = env('MDW_PAYMENTS_ENDPOINT', 'https://mdw.neolifeafrica.info/banks/webpayments/create');

        return view('payments.mdw.pay', $data);
    }

    public function return()
    {
        return view('payments.mdw.error', ['error' => 'Payment was cancelled. Please click on the link again to attempt Payment.']);
    }

    public function callback(Request $request)
    {
        $httpRequestLog = HttpRequestLog::logHttpRequest($request);

        if (!request('Status') || !request('OrderId') || !request('Reference')) {
            return view('payments.mdw.error', ['error' => 'Invalid Request']);
        }

        $orderReference = request('OrderId');
        $order = Order::whereReference($orderReference)->pending()->first();
        $reference = request('Reference');

        if (request('Status') == 'success') {
            if ($order) {
                $httpRequestLog->update(['order_id' => $order->id]);
                if ($this->payForOrder($order, $reference)) {
                    return view('payments.mdw.success', ['message' => 'Payment was Successful. Order '.$orderReference.' has been sucessfully completed.']);
                } else {
                    return view('payments.mdw.success', ['message' => 'Payment was Successful. But order was not updated. Please contact NeoLife immediately.']);
                }
            }

            _order_log($order, 'Payment successfully paid for via MDW payments');

            return view('payments.mdw.error', ['error' => __('errors.mdw_invalid_order')]);
        } else {
            _order_log($order, 'Payment attempt made. Status : '.request('status'));
        }

        return view('payments.mdw.error', ['error' => 'Payment Failed: '.request('Error')]);
    }
}
