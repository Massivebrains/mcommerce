<?php

namespace App\Http\Controllers\Api\Payments\Validation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Order;
use App\PaymentProvider;

class OrderAmountController extends Controller
{
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'id'		=> 'required|exists:orders,reference',
            'key'		=> 'required|exists:payment_providers,api_token'
        ]);

        if ($validator->fails()) {
            return response()->json([

                'status' 	=> 'error',
                'message'	=> $validator->errors()->first(),
                'data'		=> null

            ]);
        }

        $order = Order::whereReference(request('id'))->first();
        $payment_provider = PaymentProvider::whereApiToken(request('key'))->first();

        _order_log($order, $payment_provider->name.' Attempted to confirm order amount.');

        if ($order->status != 'pending') {
            _order_log($order, 'Order is no more pending');

            return response()->json([

                'status' 	=> 'error',
                'message'	=> 'Order already paid for.',
                'data'		=> null

            ], 400);
        }

        $amount = $order->total;

        if ($order->payment_provider_id == $payment_provider->id) {
            $amount -= $order->payment_provider->charge;
        }

        _order_log($order, 'Order amount retrieved successfully. Amount sent to '.$payment_provider->name.': '.$amount);

        return response()->json([

            'status' 	=> 'success',
            'message'	=> 'Order amount retrieved successfully',
            'data'		=> [

                'amount' => $amount
            ]

        ]);
    }
}
