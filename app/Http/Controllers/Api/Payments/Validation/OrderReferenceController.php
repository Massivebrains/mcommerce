<?php

namespace App\Http\Controllers\Api\Payments\Validation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Order;
use App\PaymentProvider;

class OrderReferenceController extends Controller
{
    public function __invoke(Request $request)
	{
		$validator = Validator::make($request->all(), [

			'id' 	=> 'required|exists:orders,reference',
			'key'	=> 'required|exists:payment_providers,api_token'
		]);

		if($validator->fails()){

			return response()->json([

				'status' 	=> 'error',
				'message'	=> $validator->errors()->first(),
				'data'		=> null

			]);
		}

		$payment_provider 	= PaymentProvider::whereApiToken(request('key'))->first();
		$order 				= Order::whereReference(request('id'))->first();

		_order_log($order, "{$payment_provider->name} Attempted to confirm order reference");

		if($order->status != 'pending'){

			_order_log($order, 'Order is no more pending');

			return response()->json([

				'status' 	=> 'error',
				'message'	=> 'Order already paid for.',
				'data'		=> null

			]);
		}

		return response()->json([

			'status' 	=> 'success',
			'message'	=> 'Order ID is valid',
			'data'		=> [

				'amount' => $order->total
			]

		]);
	}
}
