<?php

namespace App\Http\Controllers;

use App\Order;
use App\PaymentProvider;
use Illuminate\Http\Request;
use Validator;

class WebPaymentController extends Controller
{
    public function pay(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'private_key' => 'required',
            'order_id' => 'required',
            'phone' => 'required',
            'amount' => 'required|numeric:gt:0',
            'description' => '',
            'callback_url' => 'required|url',
            'return_url' => 'required|url',
        ]);

        $error = $validator->errors()->first() ?? null;

        if (request('private_key') != config('app.WEB_PRIVATE_KEY')) {
            $error = 'Invalid Private Key';
        }

        if ($error) {
            return view('web.error', ['error' => $error]);
        }

        $payment = Order::whereReference(request('order_id'))->web()->first();

        if (!$payment) {

            $payment = Order::create([

                'reference' => request('order_id'),
                'name' => request('phone'),
                'total' => request('amount'),
                'country_code' => 'NG',
                'callback_url' => request('callback_url'),
                'return_url' => request('return_url'),
                'type' => 'web',
                'status' => 'pending',
            ]);

            _order_log($payment, 'Created');
        }

        $data['payment'] = $payment;
        $data['providers'] = PaymentProvider::whereCountryCode('NG')->get();

        return view('web.pay', $data);
    }

    public function form()
    {
        return view('web.form');
    }

    public function requery($id = '', $simulate = '')
    {
        $payment = Order::find($id);

        if (!$payment) {

            return response()->json(['status' => false, 'data' => 'Payment not found']);
        }

        if ($simulate == 'test' && env('APP_ENV') == 'local') {

            $payment->update([

                'status' => 'paid',
                'payment_provider_id' => 1,
                'transaction_reference' => mt_rand(1111111111, 9999999999),
                'description' => 'paid',
                'paid_at' => date('Y-m-d H:i:s'),
            ]);

            _order_log($payment, 'Successfully Paid.');
        }

        if ($payment->status == 'paid') {

            $firstCharacter = strpos($payment->callback_url, '?') !== false ? '&' : '?';

            $url = $payment->callback_url;
            $url .= $firstCharacter . 'status=1';
            $url .= '&orderid=' . $payment->reference;
            $url .= '&amount=' . $payment->amount;
            $url .= '&source=' . $payment->payment_provider->name;
            $url .= '&reference=' . $payment->transaction_reference;
            $url .= '&date=' . $payment->paid_at;
            $url .= '&description=' . $payment->description;

            _order_log($payment, 'Redirected to Merchant: ' . $url);

            return response()->json(['status' => true, 'data' => $url]);
        }

        return response()->json(['status' => false, 'data' => 'Payment is still pending']);
    }

    public function noscript()
    {
        echo 'This page requires Javascript Enabled in your browser.';
    }

    public function ussdLink()
    {
        $ussd = request('code');

        if (!$ussd) {
            return redirect('tel://09032078628');
        }

        return redirect('tel://' . $ussd);
    }
}
